# 1.0.0 (Jan 16, 2019)

## 1.6.2 (Apr 9, 2019)

* Dependencies updated.

## 1.6.1 (Mar 26, 2019)

* Fix `Chart` bug with `null`s in range;
* Dependencies updated.

## 1.6.0 (Mar 26, 2019)

* Rain is considered as the sum.

## 1.5.2 (Mar 20, 2019)

* Dependencies updated.
* `AppInfo` fix.

## 1.5.1 (Mar 20, 2019)

* Increased server timeout (25 sec).

## 1.5.0 (Mar 20, 2019)

* Aggregate metrics without zeros;
* Export only one metrics.

## 1.4.4 (Jan 28, 2019)

* `browserslist` fix.

## 1.4.3 (Jan 28, 2019)

* Dependencies updated.

## 1.4.2 (Jan 28, 2019)

* Increased server timeout (10 sec).

## 1.4.1 (Jan 22, 2019)

* Added shadows to ant cards.

## 1.4.0 (Jan 22, 2019)

* Added `ErrorBoundary`.

## 1.3.0 (Jan 22, 2019)

* Added server timeout.

## 1.2.0 (Jan 22, 2019)

* Added AppInfo tab.

## 1.1.0 (Jan 22, 2019)

* Added charts.

## 1.0.2 (Jan 16, 2019)

* Short title for Excel button.

## 1.0.1 (Jan 16, 2019)

* Dependencies updated.

## 1.0.0 (Jan 16, 2019)

* MeteoData is released.
