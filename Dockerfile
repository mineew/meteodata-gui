FROM node

WORKDIR /meteodata-gui

COPY package.json yarn.lock ./
RUN yarn

COPY . ./

CMD ["yarn", "pipeline"]
