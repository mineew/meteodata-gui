const { createApp } = require('meteodata-backend')
const config = require('../src/config')
const isDev = process.argv.includes('dev')

const app = createApp({
  graphiql: isDev,
  corsOrigin: isDev ? config.reactDevUrl : undefined
})

const server = app.listen(config.serverPort, () => {
  process.send({
    success: true,
    port: config.serverPort
  })
})

server.on('error', (error) => {
  process.send({ errorMessage: error.message })
})
