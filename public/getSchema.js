/* eslint-disable no-console */

const fs = require('fs')
const { promisify } = require('util')
const fetch = require('node-fetch')
const { createApp } = require('meteodata-backend')
const config = require('../src/config')

const writeFile = promisify(fs.writeFile)

const query = `
{
  __schema {
    types {
      kind
      name
      possibleTypes {
        name
      }
    }
  }
}
`

const filename = './src/fragmentTypes.json'

function logExists () {
  console.log()
  console.log(`[*] The possible schema types are already loaded.`)
  console.log()
}

function logSuccess () {
  console.log()
  console.log(`[✓] The possible schema types has just downloaded in:`)
  console.log(filename)
  console.log()
}

function logError (error) {
  console.log()
  console.log(`[!] An error occurred while downloading the schema:`)
  console.log(error.message)
  console.log()
}

async function getSchema () {
  const app = createApp()
  const server = app.listen(config.serverPort)
  try {
    const response = await fetch(config.serverUrl, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query })
    })
    const json = await response.json()
    json.data.__schema.types = json.data.__schema.types.filter(
      (type) => type.possibleTypes !== null
    )
    const data = JSON.stringify(json.data, undefined, 2)
    await writeFile(filename, data)
    logSuccess()
    server.close()
  } catch (error) {
    logError(error)
    server.close()
    process.exit(1)
  }
}

if (fs.existsSync(filename)) {
  logExists()
} else {
  getSchema()
}
