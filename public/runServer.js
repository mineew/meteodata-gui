/* eslint-disable no-console */

const pt = require('path')
const { app, dialog } = require('electron')
const { fork } = require('child_process')
const isDev = require('electron-is-dev')
const config = require('../src/config')

function runServer (cb) {
  const serverProcess = fork(
    pt.resolve(__dirname, 'server.js'),
    [isDev ? 'dev' : 'prod']
  )

  app.on('browser-window-created', (e, window) => {
    let serverStarted = false
    setTimeout(() => {
      if (!serverStarted) {
        serverProcess.kill('SIGINT')
        showError(window, 'Не удалось запустить GraphQL сервер.')
      }
    }, config.serverTimeout)
    serverProcess.on('message', (message) => {
      const { success, port, errorMessage } = message
      if (success) {
        serverStarted = true
        if (isDev) {
          logSuccess()
        }
        global.port = port
        cb()
      } else {
        showError(window, errorMessage)
      }
    })
    serverProcess.on('error', (error) => {
      showError(window, error.message)
    })
  })

  app.on('before-quit', () => {
    if (!serverProcess.killed) {
      serverProcess.kill('SIGINT')
    }
  })
}

module.exports = runServer

/// ----------------------------------------------------------------------------

function showError (window, message) {
  const options = {
    type: 'error',
    title: 'Ошибка',
    buttons: ['Выход'],
    message
  }
  dialog.showMessageBox(window, options, () => {
    app.quit()
  })
}

function logSuccess () {
  console.log()
  console.log('Running a GraphQL API server at:')
  console.log(config.serverUrl)
  console.log()
}
