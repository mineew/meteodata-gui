const pt = require('path')
const { app, BrowserWindow } = require('electron')
const isDev = require('electron-is-dev')
const config = require('../src/config')
const { productName, version } = require('../package.json')

let mainWindow

function createMainWindow () {
  mainWindow = new BrowserWindow({
    title: `${productName} - v${version}`,
    width: 900,
    height: 700,
    show: false
  })
  if (isDev) {
    mainWindow.loadURL(config.reactDevUrl)
  } else {
    mainWindow.loadFile(pt.resolve(__dirname, config.reactProdFile))
  }
  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createMainWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createMainWindow()
  }
})

/// ----------------------------------------------------------------------------

require('./runServer')(() => {
  mainWindow.show()
  if (isDev) {
    mainWindow.webContents.openDevTools({
      mode: 'undocked'
    })
  }
})
