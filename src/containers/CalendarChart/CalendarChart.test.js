import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import * as notifications from '../../notifications'
import { metricsNames } from '../../config'
import {
  GET_CHART_MONTH, GET_CHART_YEAR, CalendarChart
} from './CalendarChart'

notifications.errorOccurred = jest.fn()

const dataItem = { name: 'name', min: 0, mean: 1, max: 2 }

const normalMocks = [{
  request: {
    query: GET_CHART_MONTH,
    variables: {
      year: 2019,
      month: 1,
      metricsName: metricsNames[0][1]
    }
  },
  result: {
    data: {
      chartMonth: {
        fullTime: [dataItem],
        lightTime: [dataItem],
        darkTime: [dataItem]
      }
    }
  }
}, {
  request: {
    query: GET_CHART_YEAR,
    variables: {
      year: 2019,
      month: 1,
      metricsName: metricsNames[0][1]
    }
  },
  result: {
    data: {
      chartYear: {
        fullTime: [dataItem],
        lightTime: [dataItem],
        darkTime: [dataItem]
      }
    }
  }
}]

const errorMocks = [{
  ...normalMocks[0],
  result: undefined,
  error: new Error('some error message')
}, {
  ...normalMocks[1],
  result: undefined,
  error: new Error('some error message')
}]

const getMonthWrapper = (mocks = normalMocks) => mount(
  <MockedProvider mocks={mocks} addTypename={false}>
    <CalendarChart
      metricsName={metricsNames[0][1]}
      type="MONTH"
      year={2019}
      month={1}
    />
  </MockedProvider>
)

const getYearWrapper = (mocks = normalMocks) => mount(
  <MockedProvider mocks={mocks} addTypename={false}>
    <CalendarChart
      metricsName={metricsNames[0][1]}
      type="YEAR"
      year={2019}
      month={1}
    />
  </MockedProvider>
)

afterEach(() => {
  notifications.errorOccurred.mockClear()
})

describe('<CalendarChart />', () => {
  it('shows the initial and loading states', () => {
    /** type: MONTH */
    const monthWrapper = getMonthWrapper()
    expect(monthWrapper.html()).toMatch('ant-spin-spinning')
    monthWrapper.unmount()
    /** type: YEAR */
    const yearWrapper = getYearWrapper()
    expect(yearWrapper.html()).toMatch('ant-spin-spinning')
    yearWrapper.unmount()
  })

  it('shows the final state', async () => {
    /** type: MONTH */
    const monthWrapper = getMonthWrapper()
    await waitForExpect(() => {
      expect(monthWrapper.html()).not.toMatch('ant-spin-spinning')
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      expect(monthWrapper.html()).toMatch('Chart-toolbox')
    })
    monthWrapper.unmount()
    /** type: YEAR */
    const yearWrapper = getYearWrapper()
    await waitForExpect(() => {
      expect(yearWrapper.html()).not.toMatch('ant-spin-spinning')
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      expect(yearWrapper.html()).toMatch('Chart-toolbox')
    })
    yearWrapper.unmount()
  })

  it('shows the error state', async () => {
    /** type: MONTH */
    const monthWrapper = getMonthWrapper(errorMocks)
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
    monthWrapper.unmount()
    /** type: YEAR */
    const yearWrapper = getYearWrapper(errorMocks)
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(2)
    })
    yearWrapper.unmount()
  })
})
