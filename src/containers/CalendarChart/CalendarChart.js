import React from 'react'
import PropTypes from 'prop-types'
import { Spin } from 'antd'
import { Center, Chart } from '../../components'
import { errorOccurred } from '../../notifications'
import './CalendarChart.css'

import gql from 'graphql-tag.macro'
import { Query } from 'react-apollo'

export const GET_CHART_YEAR = gql`
  query ChartYear ($year: Int!, $metricsName: String!) {
    chartYear(year: $year, metricsName: $metricsName) {
      fullTime {
        name
        mean
        min
        max
      }
      lightTime {
        name
        mean
        min
        max
      }
      darkTime {
        name
        mean
        min
        max
      }
    }
  }
`

export const GET_CHART_MONTH = gql`
  query ChartMonth ($year: Int!, $month: Int!, $metricsName: String!) {
    chartMonth(year: $year, month: $month, metricsName: $metricsName) {
      fullTime {
        name
        mean
        min
        max
      }
      lightTime {
        name
        mean
        min
        max
      }
      darkTime {
        name
        mean
        min
        max
      }
    }
  }
`

/**
 * @typedef {Object} CalendarChartProps
 * @property {string} metricsName
 * @property {'YEAR'|'MONTH'} type
 * @property {number} year
 * @property {number} [month]
 */

/**
 * @param {CalendarChartProps} props
 * @returns {React.Component<CalendarChartProps>}
 */
export function CalendarChart (props) {
  const { metricsName, type, year, month } = props

  return (
    <Query
      query={type === 'YEAR' ? GET_CHART_YEAR : GET_CHART_MONTH}
      variables={{ year, month, metricsName }}
      onError={errorOccurred}
    >
      {({ error, loading, data }) => (
        <div className="CalendarChart">
          {loading && (
            <Center>
              <Spin spinning={true} tip="Подождите, выполняются расчеты..." />
            </Center>
          )}
          {!error && !loading && (
            <Chart
              titles={['Сутки', 'Светлое время суток', 'Темное время суток']}
              colors={['#67809f', '#e67e22', '#2e3131']}
              data={[
                data[type === 'YEAR' ? 'chartYear' : 'chartMonth'].fullTime,
                data[type === 'YEAR' ? 'chartYear' : 'chartMonth'].lightTime,
                data[type === 'YEAR' ? 'chartYear' : 'chartMonth'].darkTime
              ]}
              metricsName={metricsName}
            />
          )}
        </div>
      )}
    </Query>
  )
}

CalendarChart.propTypes = {
  metricsName: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['YEAR', 'MONTH']).isRequired,
  year: PropTypes.number.isRequired,
  month: PropTypes.number
}

export default CalendarChart
