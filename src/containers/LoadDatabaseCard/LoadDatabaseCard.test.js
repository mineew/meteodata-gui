import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import * as notifications from '../../notifications'
import { LOAD_DATABASE, LoadDatabaseCard } from './LoadDatabaseCard'

import moment from 'moment'

notifications.errorOccurred = jest.fn()

const normalMocks = [{
  request: {
    query: LOAD_DATABASE,
    variables: {
      years: [2010, 2011, 2012]
    }
  },
  result: {
    data: {
      loadDatabase: {
        filesCount: 100,
        metricsCount: 1100,
        missedMetricsCount: 3,
        brokenMetricsCount: 11,
        minDate: moment('01.01.2010', 'DD.MM.YYYY').unix(),
        maxDate: moment('31.12.2012', 'DD.MM.YYYY').unix()
      }
    }
  }
}]

const errorMocks = [{
  request: {
    query: LOAD_DATABASE,
    variables: {
      years: [2010, 2011, 2012]
    }
  },
  error: new Error('some error message')
}]

describe('<LoadDatabaseCard />', () => {
  it('shows the initial state', () => {
    mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <LoadDatabaseCard
          years={[2010, 2011, 2012]}
          onDatabaseLoaded={jest.fn()}
        />
      </MockedProvider>
    )
  })

  it('shows the loading state', () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <LoadDatabaseCard
          years={[2010, 2011, 2012]}
          onDatabaseLoaded={jest.fn()}
        />
      </MockedProvider>
    )
    expect(wrapper.html()).not.toMatch('ant-spin-spinning')
    const loadBtn = wrapper.find('.ant-btn-primary')
    loadBtn.simulate('click')
    expect(wrapper.html()).toMatch('ant-spin-spinning')
  })

  it('shows the final state', async () => {
    const onDatabaseLoaded = jest.fn()
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <LoadDatabaseCard
          years={[2010, 2011, 2012]}
          onDatabaseLoaded={onDatabaseLoaded}
        />
      </MockedProvider>
    )
    const card = wrapper.find('LoadDatabaseCard')
    card.instance().handleCheckboxesChange([2010, 2011, 2012])
    const loadBtn = wrapper.find('.ant-btn-primary')
    loadBtn.simulate('click')
    await waitForExpect(() => {
      expect(onDatabaseLoaded).toHaveBeenCalledTimes(1)
      expect(onDatabaseLoaded).toHaveBeenLastCalledWith({
        ...normalMocks[0].result.data.loadDatabase,
        minDate: moment.unix(normalMocks[0].result.data.loadDatabase.minDate),
        maxDate: moment.unix(normalMocks[0].result.data.loadDatabase.maxDate),
        years: [2010, 2011, 2012]
      })
    })
  })

  it('shows the error state', async () => {
    const wrapper = mount(
      <MockedProvider mocks={errorMocks} addTypename={false}>
        <LoadDatabaseCard
          years={[2010, 2011, 2012]}
          onDatabaseLoaded={jest.fn()}
        />
      </MockedProvider>
    )
    const loadBtn = wrapper.find('.ant-btn-primary')
    loadBtn.simulate('click')
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
  })
})
