import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'antd'
import { LoadingCard, Checkboxes, QuitButton } from '../../components'
import { errorOccurred } from '../../notifications'

import moment from 'moment'

import gql from 'graphql-tag.macro'
import { Mutation } from 'react-apollo'

export const LOAD_DATABASE = gql`
  mutation LoadDatabase ($years: [Int!]!) {
    loadDatabase(years: $years) {
      filesCount
      metricsCount
      missedMetricsCount
      brokenMetricsCount
      minDate
      maxDate
    }
  }
`

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {Object} DatabaseInfo
 * @property {Array.<number>} years
 * @property {number} filesCount
 * @property {number} metricsCount
 * @property {number} missedMetricsCount
 * @property {number} brokenMetricsCount
 * @property {Moment} minDate
 * @property {Moment} maxDate
 */

/**
 * @typedef {Object} LoadDatabaseCardProps
 * @property {Array.<number>} years
 * @property {function(DatabaseInfo)} onDatabaseLoaded
 */

/**
 * @augments {React.Component<LoadDatabaseCardProps>}
 */
export class LoadDatabaseCard extends PureComponent {
  state = {
    selectedYears: this.props.years
  }

  render () {
    const { years } = this.props
    const { selectedYears } = this.state

    const yearsOptions = years.map((y) => y.toString())
    const yearsValue = selectedYears.map((y) => y.toString())

    return (
      <Mutation
        mutation={LOAD_DATABASE}
        onCompleted={this.handleCompleted}
        onError={errorOccurred}
      >
        {(fireMutation, { loading }) => (
          <LoadingCard
            title="Выберите, что загружать"
            icon="calendar"
            loading={loading}
            loadingTip="Подождите, выполняется чтение файлов..."
            actions={[
              <Button
                type="primary"
                icon="double-right"
                onClick={() => this.handleClick(fireMutation)}
              >
                Загрузить
              </Button>,
              <QuitButton />
            ]}
          >
            <Checkboxes
              showCheckAll={true}
              options={yearsOptions}
              value={yearsValue}
              onChange={this.handleCheckboxesChange}
            />
          </LoadingCard>
        )}
      </Mutation>
    )
  }

  handleCompleted = (data) => {
    const { onDatabaseLoaded } = this.props
    const { selectedYears } = this.state
    onDatabaseLoaded({
      years: selectedYears,
      ...data.loadDatabase,
      minDate: moment.unix(data.loadDatabase.minDate),
      maxDate: moment.unix(data.loadDatabase.maxDate)
    })
  }

  handleCheckboxesChange = (checkedOptions) => {
    this.setState({
      selectedYears: checkedOptions.map((opt) => Number(opt))
    })
  }

  handleClick = (fireMutation) => {
    const { selectedYears: years } = this.state
    fireMutation({ variables: { years } })
  }
}

LoadDatabaseCard.propTypes = {
  years: PropTypes.arrayOf(PropTypes.number).isRequired,
  onDatabaseLoaded: PropTypes.func.isRequired
}

export default LoadDatabaseCard
