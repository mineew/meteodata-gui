import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import * as notifications from '../../notifications'
import { SET_ROOT, SetRootCard } from './SetRootCard'
const { dialog } = window.require('electron').remote

notifications.errorOccurred = jest.fn()

const normalMocks = [{
  request: {
    query: SET_ROOT,
    variables: {
      path: '/some/root'
    }
  },
  result: {
    data: {
      setRoot: {
        filesCount: 100,
        invalidPaths: [],
        years: [2016, 2017, 2018]
      }
    }
  }
}]

const errorMocks = [{
  request: {
    query: SET_ROOT,
    variables: {
      path: '/some/root'
    }
  },
  error: new Error('some error message')
}]

beforeAll(() => {
  dialog.showOpenDialogPaths = ['/some/root']
})

describe('<SetRootCard />', () => {
  it('shows the initial state', () => {
    mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <SetRootCard onSetRoot={jest.fn()} />
      </MockedProvider>
    )
  })

  it('shows the loading state', () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <SetRootCard onSetRoot={jest.fn()} />
      </MockedProvider>
    )
    expect(wrapper.html()).not.toMatch('ant-spin-spinning')
    const openBtn = wrapper.find('.ant-btn-primary')
    openBtn.simulate('click')
    expect(wrapper.html()).toMatch('ant-spin-spinning')
  })

  it('shows the final state', async () => {
    const onSetRoot = jest.fn()
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <SetRootCard onSetRoot={onSetRoot} />
      </MockedProvider>
    )
    const openBtn = wrapper.find('.ant-btn-primary')
    openBtn.simulate('click')
    await waitForExpect(() => {
      expect(onSetRoot).toHaveBeenCalledTimes(1)
      expect(onSetRoot).toHaveBeenLastCalledWith({
        ...normalMocks[0].result.data.setRoot,
        root: '/some/root'
      })
    })
  })

  it('shows the error state', async () => {
    const wrapper = mount(
      <MockedProvider mocks={errorMocks} addTypename={false}>
        <SetRootCard onSetRoot={jest.fn()} />
      </MockedProvider>
    )
    const openBtn = wrapper.find('.ant-btn-primary')
    openBtn.simulate('click')
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
  })
})
