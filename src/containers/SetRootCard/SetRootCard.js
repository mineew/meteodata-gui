import React from 'react'
import PropTypes from 'prop-types'
import { LoadingCard, OpenButton, QuitButton } from '../../components'
import { errorOccurred } from '../../notifications'

import gql from 'graphql-tag.macro'
import { Mutation } from 'react-apollo'

export const SET_ROOT = gql`
  mutation SetRoot($path: String!) {
    setRoot(root: $path) {
      filesCount
      invalidPaths
      years
    }
  }
`

/**
 * @typedef {Object} RootInfo
 * @property {string} root
 * @property {number} filesCount
 * @property {Array.<string>} invalidPaths
 * @property {Array.<number>} years
 */

/**
 * @typedef {Object} SetRootCardProps
 * @property {function(RootInfo)} onSetRoot
 */

/**
 * @param {SetRootCardProps} props
 * @returns {React.Component.<SetRootCardProps>}
 */
export function SetRootCard (props) {
  const { onSetRoot } = props

  let root = ''

  const handleSelectPath = (path, fireMutation) => {
    root = path
    fireMutation({ variables: { path } })
  }

  const handleCompleted = (data) => {
    onSetRoot({ root, ...data.setRoot })
  }

  return (
    <Mutation
      mutation={SET_ROOT}
      onCompleted={handleCompleted}
      onError={errorOccurred}
    >
      {(fireMutation, { loading }) => (
        <LoadingCard
          title="Измерения не загружены"
          icon="warning"
          loading={loading}
          loadingTip="Подождите, выполняется поиск файлов..."
          actions={[
            <OpenButton
              onSelectPath={(path) => handleSelectPath(path, fireMutation)}
            />,
            <QuitButton />
          ]}
        >
          <p className="SetRootCard-content">
            Откройте папку с <code>.DAT</code> файлами.
          </p>
        </LoadingCard>
      )}
    </Mutation>
  )
}

SetRootCard.propTypes = {
  onSetRoot: PropTypes.func.isRequired
}

export default SetRootCard
