import * as containersModule from './index'

describe('`containers` module', () => {
  it('exports as expected', () => {
    const contents = Object.keys(containersModule)
    contents.sort()
    expect(contents).toEqual([
      'Calendar',
      'LoadDatabaseCard',
      'SetLatLongCard',
      'SetRootCard',
      'Table'
    ])
  })
})
