import React from 'react'
import PropTypes from 'prop-types'
import { SaveExcelButton } from '../../components'
import { errorOccurred } from '../../notifications'

import gql from 'graphql-tag.macro'
import { Mutation } from 'react-apollo'

export const EXPORT_TABLE = gql`
  mutation ExportTable (
    $start: Int!,
    $end: Int!,
    $path: String!,
    $grouping: Int,
    $addHours: Int,
    $metricsName: String
  ) {
    exportTable(
      start: $start,
      end: $end,
      path: $path,
      grouping: $grouping,
      addHours: $addHours,
      metricsName: $metricsName,
      open: true
    )
  }
`

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {Object} ExportTableButtonProps
 * @property {Moment} start
 * @property {Moment} end
 * @property {number} [grouping]
 * @property {number} [addHours]
 * @property {string} [metricsName]
 * @property {function} [onCompleted]
 */

/**
 * @param {ExportTableButtonProps} props
 * @returns {React.Component<ExportTableButtonProps>}
 */
export function ExportTableButton (props) {
  const { start, end, grouping, addHours, metricsName, onCompleted } = props
  const filename = _getFileName(start, end, grouping, addHours, metricsName)

  const handleSelectPath = (path, fireMutation) => {
    fireMutation({
      variables: {
        start: start.unix(),
        end: end.unix(),
        path,
        grouping,
        addHours,
        metricsName
      }
    })
  }

  return (
    <Mutation
      mutation={EXPORT_TABLE}
      onCompleted={onCompleted}
      onError={errorOccurred}
    >
      {(fireMutation, { loading }) => (
        <SaveExcelButton
          filename={filename}
          loading={loading}
          onSelectPath={(path) => handleSelectPath(path, fireMutation)}
        />
      )}
    </Mutation>
  )
}

ExportTableButton.defaultProps = {
  grouping: 1,
  addHours: 0
}

ExportTableButton.propTypes = {
  start: PropTypes.object.isRequired,
  end: PropTypes.object.isRequired,
  grouping: PropTypes.number,
  addHours: PropTypes.number,
  metricsName: PropTypes.string,
  onCompleted: PropTypes.func
}

export default ExportTableButton

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Moment} start
 * @param {Moment} end
 * @param {number} grouping
 * @param {number} addHours
 * @param {string} [metricsName]
 * @returns {string}
 */
function _getFileName (start, end, grouping, addHours, metricsName) {
  const startf = start.format('DD-MM-YYYY')
  const endf = end.format('DD-MM-YYYY')
  const g = grouping > 1 ? `--${grouping * 10}мин` : ''
  const add = addHours ? `--+${addHours}ч` : ''
  const ext = 'xlsx'
  if (metricsName) {
    return `${startf}--${endf}${g}${add}--${metricsName}.${ext}`
  }
  return `${startf}--${endf}${g}${add}.${ext}`
}
