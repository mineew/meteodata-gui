import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import moment from 'moment'
import * as notifications from '../../notifications'
import { EXPORT_TABLE, ExportTableButton } from './ExportTableButton'
const { dialog } = window.require('electron').remote

notifications.errorOccurred = jest.fn()

const start = moment({ year: 2018, month: 1, date: 1 })
const end = moment({ year: 2018, month: 1, date: 4 })
const grouping = 3
const addHours = 1
const filename = '01-02-2018--04-02-2018.xlsx'
const filenameExtended = '01-02-2018--04-02-2018--30мин--+1ч.xlsx'

const normalMocks = [{
  request: {
    query: EXPORT_TABLE,
    variables: {
      start: start.unix(),
      end: end.unix(),
      path: filename,
      grouping: 1,
      addHours: 0,
      metricsName: 'windSpeed'
    }
  },
  result: {
    data: {
      exportTable: true
    }
  }
}]

const extendedMocks = [{
  request: {
    query: EXPORT_TABLE,
    variables: {
      start: start.unix(),
      end: end.unix(),
      path: filenameExtended,
      grouping,
      addHours,
      metricsName: 'windSpeed'
    }
  },
  result: {
    data: {
      exportTable: true
    }
  }
}]

const errorMocks = [{
  ...normalMocks[0],
  result: undefined,
  error: new Error('some error message')
}]

afterEach(() => {
  notifications.errorOccurred.mockClear()
})

describe('<ExportTableButton />', () => {
  it('shows the initial state', () => {
    mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <ExportTableButton
          start={start}
          end={end}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
  })

  it('shows the loading state', () => {
    dialog.showSaveDialogPath = filename
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <ExportTableButton
          start={start}
          end={end}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
    expect(wrapper.html()).not.toMatch('ant-btn-loading')
    const saveBtn = wrapper.find('.ant-btn-primary')
    saveBtn.simulate('click')
    expect(wrapper.html()).toMatch('ant-btn-loading')
  })

  it('shows the final state', async () => {
    dialog.showSaveDialogPath = filenameExtended
    const onCompleted = jest.fn()
    const wrapper = mount(
      <MockedProvider mocks={extendedMocks} addTypename={false}>
        <ExportTableButton
          start={start}
          end={end}
          grouping={grouping}
          addHours={addHours}
          metricsName="windSpeed"
          onCompleted={onCompleted}
        />
      </MockedProvider>
    )
    const saveBtn = wrapper.find('.ant-btn-primary')
    saveBtn.simulate('click')
    await waitForExpect(() => {
      expect(onCompleted).toHaveBeenCalledTimes(1)
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
    })
  })

  it('shows the error state', async () => {
    dialog.showSaveDialogPath = filename
    const wrapper = mount(
      <MockedProvider mocks={errorMocks} addTypename={false}>
        <ExportTableButton
          start={start}
          end={end}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
    const saveBtn = wrapper.find('.ant-btn-primary')
    saveBtn.simulate('click')
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
  })
})
