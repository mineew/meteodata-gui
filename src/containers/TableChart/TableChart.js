import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Spin, Select } from 'antd'
import { Center, Chart } from '../../components'
import { errorOccurred } from '../../notifications'
import { metricsNames } from '../../config'
import ExportTableButton from '../ExportTableButton/ExportTableButton'
import './TableChart.css'

import gql from 'graphql-tag.macro'
import { Query } from 'react-apollo'

export const GET_CHART_TABLE = gql`
  query ChartTable (
    $start: Int!,
    $end: Int!,
    $metricsName: String!,
    $grouping: Int,
    $addHours: Int
  ) {
    chartTable(
      start: $start,
      end: $end,
      metricsName: $metricsName,
      grouping: $grouping,
      addHours: $addHours
    ) {
      name
      mean
      min
      max
    }
  }
`

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {Object} TableChartProps
 * @property {Moment} start
 * @property {Moment} end
 * @property {number} [grouping]
 * @property {number} [addHours]
 */

/**
 * @augments {React.PureComponent<TableChartProps>}
 */
export class TableChart extends PureComponent {
  state = {
    metricsName: metricsNames[0][1]
  }

  render () {
    const { start, end, grouping, addHours } = this.props
    const { metricsName } = this.state

    return (
      <Query
        query={GET_CHART_TABLE}
        variables={{
          start: start.unix(),
          end: end.unix(),
          metricsName,
          grouping,
          addHours
        }}
        onError={errorOccurred}
      >
        {({ error, loading, data }) => (
          <div className="TableChart">
            {loading && (
              <Center>
                <Spin
                  spinning={true}
                  tip="Подождите, выполняются расчеты..."
                />
              </Center>
            )}
            {!error && !loading && (
              <Fragment>
                <Select
                  className="TableChart-MetricsSelect"
                  value={metricsName}
                  onChange={this.handleMetricsChange}
                >
                  {metricsNames.map(([name, key]) => (
                    <Select.Option key={key} value={key}>{name}</Select.Option>
                  ))}
                </Select>
                <ExportTableButton
                  start={start}
                  end={end}
                  grouping={grouping}
                  addHours={addHours}
                  metricsName={metricsName}
                />
                <Chart
                  titles={[_metricsTitle(metricsName)]}
                  colors={['#67809f']}
                  data={[data.chartTable]}
                  metricsName={metricsName}
                />
              </Fragment>
            )}
          </div>
        )}
      </Query>
    )
  }

  handleMetricsChange = (metricsName) => {
    this.setState({ metricsName })
  }
}

TableChart.defaultProps = {
  grouping: 1,
  addHours: 0
}

TableChart.propTypes = {
  start: PropTypes.object.isRequired,
  end: PropTypes.object.isRequired,
  grouping: PropTypes.number,
  addHours: PropTypes.number
}

export default TableChart

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {string} metricsName
 * @returns {string}
 */
function _metricsTitle (metricsName) {
  for (let metricsData of metricsNames) {
    if (metricsData[1] === metricsName) {
      return metricsData[0]
    }
  }
}
