import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import moment from 'moment'
import * as notifications from '../../notifications'
import { metricsNames } from '../../config'
import { GET_CHART_TABLE, TableChart } from './TableChart'

notifications.errorOccurred = jest.fn()

const dataItem = { name: 'name', min: 0, mean: 1, max: 2 }
const start = moment({ year: 2018, month: 1, date: 1 })
const end = moment({ year: 2018, month: 1, date: 4 })

const normalMocks = [{
  request: {
    query: GET_CHART_TABLE,
    variables: {
      start: start.unix(),
      end: end.unix(),
      metricsName: metricsNames[0][1],
      grouping: 1,
      addHours: 0
    }
  },
  result: {
    data: {
      chartTable: [dataItem]
    }
  }
}, {
  request: {
    query: GET_CHART_TABLE,
    variables: {
      start: start.unix(),
      end: end.unix(),
      metricsName: metricsNames[1][1],
      grouping: 1,
      addHours: 0
    }
  },
  result: {
    data: {
      chartTable: [dataItem]
    }
  }
}]

const errorMocks = [{
  ...normalMocks[0],
  result: undefined,
  error: new Error('some error message')
}]

const getWrapper = (mocks = normalMocks) => mount(
  <MockedProvider mocks={mocks} addTypename={false}>
    <TableChart
      start={start}
      end={end}
      grouping={1}
      addHours={0}
    />
  </MockedProvider>
)

afterEach(() => {
  notifications.errorOccurred.mockClear()
})

describe('<TableChart />', () => {
  it('shows the initial and loading states', () => {
    const wrapper = getWrapper()
    expect(wrapper.html()).toMatch('ant-spin-spinning')
    wrapper.unmount()
  })

  it('shows the final state', async () => {
    const wrapper = getWrapper()
    await waitForExpect(() => {
      expect(wrapper.html()).not.toMatch('ant-spin-spinning')
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      expect(wrapper.html()).toMatch('Chart-toolbox')
    })
    wrapper.unmount()
  })

  it('shows the error state', async () => {
    const wrapper = getWrapper(errorMocks)
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
    wrapper.unmount()
  })

  it('can change metrics', async () => {
    const wrapper = getWrapper()
    wrapper.find('TableChart').instance().handleMetricsChange(
      metricsNames[1][1]
    )
    await waitForExpect(() => {
      expect(wrapper.html()).not.toMatch('ant-spin-spinning')
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      expect(wrapper.html()).toMatch('Chart-toolbox')
    })
    wrapper.unmount()
  })
})
