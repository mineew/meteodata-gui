import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import moment from 'moment'
import { createEmptyMetricsData } from 'meteodata-backend'
import { cache } from '../../client'
import * as notifications from '../../notifications'
import { GET_CALENDAR_DATA, Calendar } from './Calendar'

notifications.errorOccurred = jest.fn()

const monthMocks = [{
  request: {
    query: GET_CALENDAR_DATA,
    variables: {
      type: 'MONTH',
      year: 2019,
      month: 1
    }
  },
  result: {
    data: {
      calendar: [{
        day: 1,
        totalCount: 100,
        missedCount: 50,
        brokenCount: 25,
        lightTime: createEmptyMetricsData().fill(1),
        darkTime: createEmptyMetricsData().fill(2),
        fullTime: createEmptyMetricsData().fill(3),
        __typename: 'CalendarDay'
      }]
    }
  }
}]

const yearMocks = [{
  request: {
    query: GET_CALENDAR_DATA,
    variables: {
      type: 'YEAR',
      year: 2019,
      month: 1
    }
  },
  result: {
    data: {
      calendar: [{
        month: 1,
        totalCount: 200,
        missedCount: 100,
        brokenCount: 50,
        lightTime: createEmptyMetricsData().fill(4),
        darkTime: createEmptyMetricsData().fill(5),
        fullTime: createEmptyMetricsData().fill(6),
        __typename: 'CalendarMonth'
      }]
    }
  }
}]

const errorMonthMocks = [{
  ...monthMocks[0],
  result: undefined,
  error: new Error('some error message')
}]

const errorYearMocks = [{
  ...yearMocks[0],
  result: undefined,
  error: new Error('some error message')
}]

const minDate = moment({ year: 2019, month: 1, date: 1 })
const maxDate = moment({ year: 2019, month: 2, date: 31 })

describe('<Calendar />', () => {
  it('shows the initial and loading states', () => {
    /** type: MONTH */
    const monthWrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    expect(monthWrapper.html()).toMatch('ant-spin-spinning')
    /** type: YEAR */
    const yearWrapper = mount(
      <MockedProvider mocks={yearMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} mode="year" />
      </MockedProvider>
    )
    expect(yearWrapper.html()).toMatch('ant-spin-spinning')
    /** unmount */
    monthWrapper.unmount()
    yearWrapper.unmount()
  })

  it('shows the final state', async () => {
    /** type: MONTH */
    const monthWrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    await waitForExpect(() => {
      expect(monthWrapper.html()).not.toMatch('ant-spin-spinning')
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      const means = monthWrapper.update().find('.CalendarCell-mean')
      expect(means.at(0).text()).toMatch('3')
      expect(means.at(1).text()).toMatch('1')
      expect(means.at(2).text()).toMatch('2')
    })
    /** type: YEAR */
    const yearWrapper = mount(
      <MockedProvider mocks={yearMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} mode="year" />
      </MockedProvider>
    )
    await waitForExpect(() => {
      expect(yearWrapper.html()).not.toMatch('ant-spin-spinning')
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      const means = yearWrapper.update().find('.CalendarCell-mean')
      expect(means.at(0).text()).toMatch('6')
      expect(means.at(1).text()).toMatch('4')
      expect(means.at(2).text()).toMatch('5')
    })
    /** unmount */
    monthWrapper.unmount()
    yearWrapper.unmount()
  })

  it('shows the error state', async () => {
    /** type: MONTH */
    const monthWrapper = mount(
      <MockedProvider mocks={errorMonthMocks} addTypename={false}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
    /** type: YEAR */
    notifications.errorOccurred.mockClear()
    const yearWrapper = mount(
      <MockedProvider mocks={errorYearMocks} addTypename={false}>
        <Calendar minDate={minDate} maxDate={maxDate} mode="year" />
      </MockedProvider>
    )
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
    /** unmount */
    monthWrapper.unmount()
    yearWrapper.unmount()
  })

  it('can change selected metrics name', () => {
    const wrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    const prevSelected = wrapper.find('.ant-menu-item-selected').text()
    wrapper.find('.ant-menu-item').at(1).simulate('click')
    const nowSelected = wrapper.find('.ant-menu-item-selected').text()
    expect(nowSelected).not.toMatch(prevSelected)
    wrapper.unmount()
  })

  it('can change calendar value', () => {
    const wrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    const selectedMonthClass = '.ant-select-selection-selected-value'
    const prevSelectedMonth = wrapper.find(selectedMonthClass).last().text()
    wrapper.find('.ant-fullcalendar-cell').last().simulate('click')
    const nowSelectedMonth = wrapper.find(selectedMonthClass).last().text()
    expect(nowSelectedMonth).not.toMatch(prevSelectedMonth)
    wrapper.unmount()
  })

  it('can change calendar mode', () => {
    const wrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    const selectedModeClass = '.ant-radio-button-wrapper-checked'
    const prevSelectedMode = wrapper.find(selectedModeClass).text()
    wrapper.find('.ant-radio-button-input').last().simulate('change')
    const nowSelectedMode = wrapper.find(selectedModeClass).text()
    expect(nowSelectedMode).not.toMatch(prevSelectedMode)
    wrapper.find('.ant-radio-button-input').first().simulate('change')
    const backSelectedMode = wrapper.find(selectedModeClass).text()
    expect(backSelectedMode).toMatch(prevSelectedMode)
    wrapper.unmount()
  })

  it('can change show values', () => {
    const wrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    expect(wrapper.find('.ant-checkbox-checked')).toHaveLength(1)
    wrapper.find('.ant-checkbox-input').at(0).simulate('change')
    expect(wrapper.find('.ant-checkbox-checked')).toHaveLength(2)
    wrapper.find('.ant-checkbox-input').at(2).simulate('change')
    expect(wrapper.find('.ant-checkbox-checked')).toHaveLength(3)
    wrapper.find('.ant-checkbox-input').at(1).simulate('change')
    expect(wrapper.find('.ant-checkbox-checked')).toHaveLength(2)
    wrapper.find('.ant-checkbox-input').at(0).simulate('change')
    expect(wrapper.find('.ant-checkbox-checked')).toHaveLength(1)
    wrapper.unmount()
  })

  it('cannot disable all show values', () => {
    const wrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    expect(wrapper.find('.ant-checkbox-checked')).toHaveLength(1)
    wrapper.find('.ant-checkbox-input').at(1).simulate('change')
    expect(wrapper.find('.ant-checkbox-checked')).toHaveLength(1)
    wrapper.unmount()
  })

  it('shows the chart button', () => {
    const wrapper = mount(
      <MockedProvider mocks={monthMocks} cache={cache}>
        <Calendar minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    expect(wrapper.find('DrawerButton')).toHaveLength(1)
    wrapper.unmount()
  })
})
