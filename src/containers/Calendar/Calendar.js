import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Layout, Calendar as AntdCalendar, Spin } from 'antd'
import { Menu, Checkboxes, CalendarCell, DrawerButton } from '../../components'
import { errorOccurred } from '../../notifications'
import { metricsNames } from '../../config'
import ExportCalendarButton from '../ExportCalendarButton/ExportCalendarButton'
import CalendarChart from '../CalendarChart/CalendarChart'
import './Calendar.css'

import moment from 'moment'

import gql from 'graphql-tag.macro'
import { Query } from 'react-apollo'

export const GET_CALENDAR_DATA = gql`
  query GetCalendarData ($type: CalendarDataType!, $year: Int!, $month: Int) {
    calendar(type: $type, year: $year, month: $month) {
      ... on CalendarDay {
        day
      }
      ... on CalendarMonth {
        month
      }
      ... on CalendarData {
        totalCount
        missedCount
        brokenCount
        fullTime
        lightTime
        darkTime
      }
    }
  }
`

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {Object} CalendarProps
 * @property {Moment} minDate
 * @property {Moment} maxDate
 * @property {'year'|'month'} [mode]
 * @property {number} [siderWidth]
 */

/**
 * @augments {React.Component<CalendarProps>}
 */
export class Calendar extends PureComponent {
  state = {
    selectedMetrics: metricsNames[0][1],
    date: this.props.minDate,
    year: this.props.minDate.year(),
    month: this.props.minDate.month(),
    type: this.props.mode === 'month' ? 'MONTH' : 'YEAR',
    showValues: ['mean']
  }

  render () {
    const { selectedMetrics, date, year, month, type, showValues } = this.state
    const { minDate, maxDate, siderWidth } = this.props

    const checkboxesOptions = selectedMetrics === 'rain'
      ? ['Сумма']
      : ['Минимум', 'Среднее', 'Максимум']

    return (
      <Layout className="Calendar">
        <Layout.Sider width={siderWidth}>
          <Menu
            options={metricsNames}
            value={selectedMetrics}
            onChange={this.handleMenuChange}
          />
        </Layout.Sider>
        <Layout.Content>
          <div className="Calendar-toolbar">
            <Checkboxes
              showCheckAll={false}
              options={checkboxesOptions}
              value={this.showValuesNames}
              onChange={this.handleCheckboxesChange}
            />
            <DrawerButton
              title="График"
              icon="line-chart"
              drawerTitle={this.chartTitle}
            >
              <CalendarChart
                type={type}
                year={year}
                month={month}
                metricsName={selectedMetrics}
              />
            </DrawerButton>
            <ExportCalendarButton
              year={year}
              month={month}
              type={type}
              metricsName={selectedMetrics}
            />
          </div>
          <Query
            query={GET_CALENDAR_DATA}
            variables={{ type, year, month }}
            onError={errorOccurred}
          >
            {({ error, loading, data }) => (
              <Spin
                spinning={loading}
                size="large"
                tip="Подождите, выполняются расчеты..."
              >
                <AntdCalendar
                  validRange={[minDate, maxDate]}
                  value={date}
                  mode={type === 'MONTH' ? 'month' : 'year'}
                  onChange={this.handleCalendarChange}
                  onPanelChange={this.handleCalendarPanelChange}
                  dateCellRender={(cellDate) => !error && !loading && (
                    <CalendarCell
                      data={_getDayData(data, cellDate, date.month()) || false}
                      metricsName={selectedMetrics}
                      show={showValues}
                    />
                  )}
                  monthCellRender={(cellDate) => !error && !loading && (
                    <CalendarCell
                      data={_getMonthData(data, cellDate) || false}
                      metricsName={selectedMetrics}
                      show={showValues}
                    />
                  )}
                />
              </Spin>
            )}
          </Query>
        </Layout.Content>
      </Layout>
    )
  }

  /** handlers */

  handleMenuChange = (selectedMetrics) => {
    this.setState({ selectedMetrics })
  }

  handleCalendarChange = (date) => {
    this.setState({
      date,
      year: date.year(),
      month: date.month()
    })
  }

  handleCalendarPanelChange = (date, mode) => {
    this.setState({
      date,
      year: date.year(),
      month: date.month(),
      type: mode === 'year' ? 'YEAR' : 'MONTH'
    })
  }

  handleCheckboxesChange = (selectedOptions) => {
    const showValues = []
    if (selectedOptions.includes('Минимум')) {
      showValues.push('min')
    }
    if (
      selectedOptions.includes('Среднее') ||
      selectedOptions.includes('Сумма')
    ) {
      showValues.push('mean')
    }
    if (selectedOptions.includes('Максимум')) {
      showValues.push('max')
    }
    if (showValues.length > 0) {
      this.setState({ showValues })
    }
  }

  /** getters */

  get showValuesNames () {
    const { showValues, selectedMetrics } = this.state
    const names = []
    if (showValues.includes('min')) {
      names.push('Минимум')
    }
    if (showValues.includes('mean')) {
      selectedMetrics === 'rain'
        ? names.push('Сумма')
        : names.push('Среднее')
    }
    if (showValues.includes('max')) {
      names.push('Максимум')
    }
    return names
  }

  get chartTitle () {
    const { type, year, month, selectedMetrics } = this.state
    let metricsName = ''
    for (let [ name, key ] of metricsNames) {
      if (key === selectedMetrics) {
        metricsName = name
        break
      }
    }
    if (type === 'YEAR') {
      return `${year} – ${metricsName}`
    }
    const monthName = moment({ year, month }).format('MMMM')
    const monthNameCap = monthName.charAt(0).toUpperCase() + monthName.slice(1)
    return `${monthNameCap} ${year} – ${metricsName}`
  }
}

Calendar.defaultProps = {
  mode: 'month',
  siderWidth: 250
}

Calendar.propTypes = {
  minDate: PropTypes.object.isRequired,
  maxDate: PropTypes.object.isRequired,
  siderWidth: PropTypes.number
}

export default Calendar

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {object} data
 * @param {Moment} cellDate
 * @param {number} displayMonth
 * @returns {object|null}
 */
function _getDayData (data, cellDate, displayMonth) {
  const { calendar } = data
  if (cellDate.month() !== displayMonth) {
    return null
  }
  let dayData = null
  for (let i = 0; i < calendar.length; i++) {
    if (calendar[i].day === cellDate.date()) {
      dayData = calendar[i]
      break
    }
  }
  return dayData
}

/**
 * @private
 * @param {object} data
 * @param {Moment} cellDate
 * @returns {object|null}
 */
function _getMonthData (data, cellDate) {
  const { calendar } = data
  let monthData = null
  for (let i = 0; i < calendar.length; i++) {
    if (calendar[i].month === cellDate.month()) {
      monthData = calendar[i]
      break
    }
  }
  return monthData
}
