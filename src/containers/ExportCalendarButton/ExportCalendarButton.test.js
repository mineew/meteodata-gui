import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import * as notifications from '../../notifications'
import { EXPORT_CALENDAR, ExportCalendarButton } from './ExportCalendarButton'
const { dialog } = window.require('electron').remote

notifications.errorOccurred = jest.fn()

const monthMocks = [{
  request: {
    query: EXPORT_CALENDAR,
    variables: {
      type: 'MONTH',
      year: 2019,
      month: 1,
      metricsName: 'windSpeed',
      path: '/some/path/2019-февраль.xlsx'
    }
  },
  result: {
    data: {
      exportCalendar: true
    }
  }
}]

const yearMocks = [{
  request: {
    query: EXPORT_CALENDAR,
    variables: {
      type: 'YEAR',
      year: 2019,
      month: undefined,
      metricsName: 'windSpeed',
      path: '/some/path/2019.xlsx'
    }
  },
  result: {
    data: {
      exportCalendar: true
    }
  }
}]

const errorMonthMocks = [{
  ...monthMocks[0],
  result: undefined,
  error: new Error('some error message')
}]

const errorYearMocks = [{
  ...yearMocks[0],
  result: undefined,
  error: new Error('some error message')
}]

describe('<ExportCalendarButton />', () => {
  it('shows the initial state', () => {
    /** type: MONTH */
    mount(
      <MockedProvider mocks={monthMocks} addTypename={false}>
        <ExportCalendarButton
          type="MONTH"
          year={2019}
          month={1}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
    /** type: YEAR */
    mount(
      <MockedProvider mocks={yearMocks} addTypename={false}>
        <ExportCalendarButton
          type="YEAR"
          year={2019}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
  })

  it('shows the loading state', () => {
    /** type: MONTH */
    dialog.showSaveDialogPath = '/some/path/2019-февраль.xlsx'
    const monthWrapper = mount(
      <MockedProvider mocks={monthMocks} addTypename={false}>
        <ExportCalendarButton
          type="MONTH"
          year={2019}
          month={1}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
    expect(monthWrapper.html()).not.toMatch('ant-btn-loading')
    const saveBtnMonth = monthWrapper.find('.ant-btn-primary')
    saveBtnMonth.simulate('click')
    expect(monthWrapper.html()).toMatch('ant-btn-loading')
    /** type: YEAR */
    dialog.showSaveDialogPath = '/some/path/2019.xlsx'
    const yearWrapper = mount(
      <MockedProvider mocks={yearMocks} addTypename={false}>
        <ExportCalendarButton
          type="YEAR"
          year={2019}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
    expect(yearWrapper.html()).not.toMatch('ant-btn-loading')
    const saveBtnYear = yearWrapper.find('.ant-btn-primary')
    saveBtnYear.simulate('click')
    expect(yearWrapper.html()).toMatch('ant-btn-loading')
    /** unmount */
    monthWrapper.unmount()
    yearWrapper.unmount()
  })

  it('shows the final state', async () => {
    /** type: MONTH */
    dialog.showSaveDialogPath = '/some/path/2019-февраль.xlsx'
    const onCompleted = jest.fn()
    const monthWrapper = mount(
      <MockedProvider mocks={monthMocks} addTypename={false}>
        <ExportCalendarButton
          type="MONTH"
          year={2019}
          month={1}
          metricsName="windSpeed"
          onCompleted={onCompleted}
        />
      </MockedProvider>
    )
    const saveBtnMonth = monthWrapper.find('.ant-btn-primary')
    saveBtnMonth.simulate('click')
    await waitForExpect(() => {
      expect(onCompleted).toHaveBeenCalledTimes(1)
    })
    /** type: YEAR */
    onCompleted.mockClear()
    dialog.showSaveDialogPath = '/some/path/2019.xlsx'
    const yearWrapper = mount(
      <MockedProvider mocks={yearMocks} addTypename={false}>
        <ExportCalendarButton
          type="YEAR"
          year={2019}
          metricsName="windSpeed"
          onCompleted={onCompleted}
        />
      </MockedProvider>
    )
    const saveBtnYear = yearWrapper.find('.ant-btn-primary')
    saveBtnYear.simulate('click')
    await waitForExpect(() => {
      expect(onCompleted).toHaveBeenCalledTimes(1)
    })
    /** unmount */
    monthWrapper.unmount()
    yearWrapper.unmount()
  })

  it('shows the error state', async () => {
    /** type: MONTH */
    const monthWrapper = mount(
      <MockedProvider mocks={errorMonthMocks} addTypename={false}>
        <ExportCalendarButton
          type="MONTH"
          year={2019}
          month={1}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
    const saveBtnMonth = monthWrapper.find('.ant-btn-primary')
    saveBtnMonth.simulate('click')
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
    /** type: YEAR */
    notifications.errorOccurred.mockClear()
    const yearWrapper = mount(
      <MockedProvider mocks={errorYearMocks} addTypename={false}>
        <ExportCalendarButton
          type="YEAR"
          year={2019}
          metricsName="windSpeed"
        />
      </MockedProvider>
    )
    const saveBtnYear = yearWrapper.find('.ant-btn-primary')
    saveBtnYear.simulate('click')
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
  })
})
