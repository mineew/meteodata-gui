import React from 'react'
import PropTypes from 'prop-types'
import { SaveExcelButton } from '../../components'
import { errorOccurred } from '../../notifications'

import moment from 'moment'

import gql from 'graphql-tag.macro'
import { Mutation } from 'react-apollo'

export const EXPORT_CALENDAR = gql`
  mutation ExportCalendar (
    $type: CalendarDataType!,
    $year: Int!,
    $month: Int,
    $path: String!,
    $metricsName: String
  ) {
    exportCalendar(
      type: $type,
      year: $year,
      month: $month,
      path: $path,
      metricsName: $metricsName,
      open: true
    )
  }
`

/**
 * @typedef {Object} ExportCalendarButtonProps
 * @property {'YEAR'|'MONTH'} type
 * @property {number} year
 * @property {number} [month]
 * @property {string} [metricsName]
 * @property {function} [onCompleted]
 */

/**
 * @param {ExportCalendarButtonProps} props
 * @returns {React.Component<ExportCalendarButtonProps>}
 */
export function ExportCalendarButton (props) {
  const { type, year, month, metricsName, onCompleted } = props

  let filename = type === 'MONTH'
    ? `${year}-${moment({ year, month }).format('MMMM')}`
    : year.toString()

  if (metricsName) {
    filename = `${filename}-${metricsName}`
  }

  const handleSelectPath = (path, fireMutation) => {
    fireMutation({ variables: { type, year, month, metricsName, path } })
  }

  return (
    <Mutation
      mutation={EXPORT_CALENDAR}
      onCompleted={onCompleted}
      onError={errorOccurred}
    >
      {(fireMutation, { loading }) => (
        <SaveExcelButton
          filename={`${filename}.xlsx`}
          loading={loading}
          onSelectPath={(path) => handleSelectPath(path, fireMutation)}
        />
      )}
    </Mutation>
  )
}

ExportCalendarButton.propTypes = {
  type: PropTypes.oneOf(['YEAR', 'MONTH']).isRequired,
  year: PropTypes.number.isRequired,
  month: PropTypes.number,
  metricsName: PropTypes.string,
  onCompleted: PropTypes.func
}

export default ExportCalendarButton
