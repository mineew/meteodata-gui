import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Table as AntdTable, DatePicker, Select, Spin } from 'antd'
import { DrawerButton } from '../../components'
import { errorOccurred } from '../../notifications'
import { metricsNames } from '../../config'
import TableChart from '../TableChart/TableChart'
import './Table.css'

import moment from 'moment'
import { parseMetricsData } from 'meteodata-backend'

import gql from 'graphql-tag.macro'
import { Query } from 'react-apollo'

export const GET_TABLE_DATA = gql`
  query GetTableData (
    $start: Int!, $end: Int!, $grouping: Int, $addHours: Int
  ) {
    table(start: $start, end: $end, grouping: $grouping, addHours: $addHours) {
      date
      time
      data
    }
  }
`

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {Object} TableProps
 * @property {Moment} minDate
 * @property {Moment} maxDate
 * @property {number} [defaultNumberOfDays]
 * @property {Array.<[number, string]>} [groupingTypes]
 * @property {Array.<[number, string]>} [addHoursTypes]
 */

/**
 * @augments {React.PureComponent<TableProps>}
 */
export class Table extends PureComponent {
  state = {
    start: _prepareTableDate(this.props.minDate),
    end: _prepareTableDate(this.props.minDate).add(
      this.props.defaultNumberOfDays, 'days'
    ),
    grouping: 1,
    addHours: 0
  }

  render () {
    const { minDate, maxDate, groupingTypes, addHoursTypes } = this.props
    const { start, end, grouping, addHours } = this.state

    return (
      <div className="Table">
        <div className="Table-toolbar">
          <DatePicker.RangePicker
            allowClear={false}
            format="DD.MM.YYYY"
            disabledDate={
              /* istanbul ignore next */
              (date) => date < minDate || date > maxDate
            }
            value={[start, end]}
            onChange={this.handleRangeChange}
          />
          <Select value={grouping} onChange={this.handleGroupingChange}>
            {groupingTypes.map(([v, n]) => (
              <Select.Option key={v} value={v}>{n}</Select.Option>
            ))}
          </Select>
          <Select value={addHours} onChange={this.handleAddHoursChange}>
            {addHoursTypes.map(([v, n]) => (
              <Select.Option key={v} value={v}>{n}</Select.Option>
            ))}
          </Select>
          <DrawerButton
            title="График"
            icon="line-chart"
            drawerTitle={this.chartTitle}
          >
            <TableChart
              start={start}
              end={end}
              grouping={grouping}
              addHours={addHours}
            />
          </DrawerButton>
        </div>
        <Query
          query={GET_TABLE_DATA}
          variables={{
            start: start.unix(),
            end: end.unix(),
            grouping,
            addHours
          }}
          onError={errorOccurred}
        >
          {({ error, loading, data }) => (
            <Spin
              spinning={loading}
              tip="Подождите, выполняются расчеты..."
              size="large"
            >
              <AntdTable
                bordered={true}
                size="small"
                scroll={{ x: true }}
                pagination={{ showSizeChanger: true }}
                dataSource={
                  error || loading ? undefined : _parseTableData(data.table)
                }
              >
                <AntdTable.Column
                  dataIndex="date" key="date" title="Дата"
                  fixed="left"
                />
                <AntdTable.Column
                  dataIndex="time" key="time" title="Время"
                  fixed="left"
                />
                {metricsNames.map(([ title, key ]) => {
                  return key !== 'rain'
                    ? _renderObjectColumn(title, key)
                    : _renderNumberColumn(title, key)
                })}
              </AntdTable>
            </Spin>
          )}
        </Query>
      </div>
    )
  }

  /** handlers */

  handleRangeChange = ([ start, end ]) => {
    this.setState({
      start: _prepareTableDate(start),
      end: _prepareTableDate(end)
    })
  }

  handleGroupingChange = (grouping) => {
    this.setState({ grouping })
  }

  handleAddHoursChange = (addHours) => {
    this.setState({ addHours })
  }

  /** getters */

  get chartTitle () {
    const { start, end } = this.state
    if (start.format('DD.MM.YYYY') === end.format('DD.MM.YYYY')) {
      return start.format('DD MMM YYYY')
    }
    if (start.format('MM.YYYY') === end.format('MM.YYYY')) {
      return `${start.format('DD')} – ${end.format('DD MMM YYYY')}`
    }
    if (start.format('YYYY') === end.format('YYYY')) {
      return `${start.format('DD MMM')} – ${end.format('DD MMM YYYY')}`
    }
    return `${start.format('DD.MM.YYYY')} – ${end.format('DD.MM.YYYY')}`
  }
}

Table.defaultProps = {
  defaultNumberOfDays: 3,
  groupingTypes: [
    [1, '10 мин'],
    [3, '30 мин'],
    [6, '1 час'],
    [12, '2 часа'],
    [18, '3 часа']
  ],
  addHoursTypes: [
    [0, '+0 часов'],
    [1, '+1 час'],
    [2, '+2 часа'],
    [3, '+3 часа']
  ]
}

Table.propTypes = {
  minDate: PropTypes.object.isRequired,
  maxDate: PropTypes.object.isRequired,
  defaultNumberOfDays: PropTypes.number,
  groupingTypes: PropTypes.arrayOf(PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  ))
}

export default Table

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Moment} date
 * @returns {Moment}
 */
function _prepareTableDate (date) {
  return moment({
    year: date.year(),
    month: date.month(),
    day: date.date()
  })
}

/**
 * @typedef {Object} TableRow
 * @property {string} date
 * @property {string} time
 * @property {Array.<number|null>} data
 */

/**
 * @private
 * @param {Array.<TableRow>} table
 * @returns {Array.<object>}
 */
function _parseTableData (table) {
  return table.map((row) => ({
    date: row.date,
    time: row.time,
    key: `${row.date} ${row.time}`,
    data: parseMetricsData(row.data.map(
      (n) => n !== null ? n.toFixed(2) : '-'
    ))
  }))
}

/**
 * @private
 * @param {string} title
 * @param {string} key
 * @returns {React.ReactNode}
 */
function _renderObjectColumn (title, key) {
  return (
    <AntdTable.ColumnGroup title={title} key={key}>
      <AntdTable.Column
        title="мин"
        dataIndex={`data.${key}.min`}
        key={`data.${key}.min`}
      />
      <AntdTable.Column
        title="средн"
        dataIndex={`data.${key}.mean`}
        key={`data.${key}.mean`}
      />
      <AntdTable.Column
        title="макс"
        dataIndex={`data.${key}.max`}
        key={`data.${key}.max`}
      />
    </AntdTable.ColumnGroup>
  )
}

/**
 * @private
 * @param {string} title
 * @param {string} key
 * @returns {React.ReactNode}
 */
function _renderNumberColumn (title, key) {
  return (
    <AntdTable.Column
      title={title}
      dataIndex={`data.${key}`}
      key={`data.${key}`}
    />
  )
}
