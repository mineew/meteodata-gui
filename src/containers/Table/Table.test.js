import React from 'react'
import { mount, shallow } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import { Select, DatePicker } from 'antd'
import moment from 'moment'
import { createEmptyMetricsData } from 'meteodata-backend'
import * as notifications from '../../notifications'
import { GET_TABLE_DATA, Table } from './Table'

notifications.errorOccurred = jest.fn()

const minDate = moment({ year: 2018, month: 1, date: 1 })
const maxDate = moment({ year: 2018, month: 1, date: 30 })
const start = moment({ year: 2018, month: 1, date: 1 })
const end = moment({ year: 2018, month: 1, date: 4 })
const data = createEmptyMetricsData().fill(150)
data[0] = null

afterEach(() => {
  notifications.errorOccurred.mockClear()
})

const normalMocks = [{
  request: {
    query: GET_TABLE_DATA,
    variables: {
      start: start.unix(),
      end: end.unix(),
      grouping: 1,
      addHours: 0
    }
  },
  result: {
    data: {
      table: [{
        date: '01.02.2018',
        time: '00:00',
        data
      }]
    }
  }
}, {
  request: {
    query: GET_TABLE_DATA,
    variables: {
      start: start.clone().add(1, 'day').unix(),
      end: end.unix(),
      grouping: 1,
      addHours: 0
    }
  },
  result: {
    data: {
      table: [{
        date: '02.02.2018',
        time: '00:00',
        data
      }]
    }
  }
}, {
  request: {
    query: GET_TABLE_DATA,
    variables: {
      start: start.unix(),
      end: end.unix(),
      grouping: 3,
      addHours: 0
    }
  },
  result: {
    data: {
      table: [{
        date: '01.02.2018',
        time: '00:30',
        data
      }]
    }
  }
}, {
  request: {
    query: GET_TABLE_DATA,
    variables: {
      start: start.unix(),
      end: end.unix(),
      grouping: 1,
      addHours: 1
    }
  },
  result: {
    data: {
      table: [{
        date: '01.02.2018',
        time: '01:00',
        data
      }]
    }
  }
}]

const errorMocks = [{
  ...normalMocks[0],
  result: undefined,
  error: new Error('some error message')
}]

describe('<Table />', () => {
  it('shows the initial and loading states', () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <Table minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    expect(wrapper.html()).toMatch('ant-spin-spinning')
  })

  it('shows the final state', async () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <Table minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    await waitForExpect(() => {
      expect(wrapper.html()).not.toMatch('ant-spin-spinning')
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      const cells = wrapper.update().find('td')
      expect(cells.at(0).text()).toMatch('01.02.2018')
      expect(cells.at(1).text()).toMatch('00:00')
      expect(cells.at(2).text()).toMatch('150')
      expect(cells.at(3).text()).toMatch('-')
    })
  })

  it('shows the error state', async () => {
    mount(
      <MockedProvider mocks={errorMocks} addTypename={false}>
        <Table minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
  })

  it('can change range', async () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <Table minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    wrapper.update().find(DatePicker.RangePicker).instance().props.onChange([
      start.clone().add(1, 'day'),
      end
    ])
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      const cells = wrapper.update().find('td')
      expect(cells.at(0).text()).toMatch('02.02.2018')
    })
  })

  it('can group metrics', async () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <Table minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    wrapper.update().find(Select).at(0).instance().props.onChange(3)
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      const cells = wrapper.update().find('td')
      expect(cells.at(1).text()).toMatch('00:30')
    })
  })

  it('can add hours', async () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <Table minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    wrapper.update().find(Select).at(1).instance().props.onChange(1)
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(0)
      const cells = wrapper.update().find('td')
      expect(cells.at(1).text()).toMatch('01:00')
    })
  })

  it('shows the chart button', () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <Table minDate={minDate} maxDate={maxDate} />
      </MockedProvider>
    )
    expect(wrapper.find('DrawerButton')).toHaveLength(1)
    wrapper.unmount()
  })

  it('formats the chart title correctly', () => {
    const wrapper = shallow(
      <Table minDate={minDate} maxDate={maxDate} />
    )
    // one day
    wrapper.instance().handleRangeChange([
      moment({ year: 2018, month: 1, date: 1 }),
      moment({ year: 2018, month: 1, date: 1 })
    ])
    expect(wrapper.instance().chartTitle).toBe('01 февр. 2018')
    // one month
    wrapper.instance().handleRangeChange([
      moment({ year: 2018, month: 1, date: 1 }),
      moment({ year: 2018, month: 1, date: 5 })
    ])
    expect(wrapper.instance().chartTitle).toBe('01 – 05 февр. 2018')
    // one year
    wrapper.instance().handleRangeChange([
      moment({ year: 2018, month: 1, date: 1 }),
      moment({ year: 2018, month: 2, date: 5 })
    ])
    expect(wrapper.instance().chartTitle).toBe('01 февр. – 05 мар. 2018')
    // default
    wrapper.instance().handleRangeChange([
      moment({ year: 2018, month: 1, date: 1 }),
      moment({ year: 2019, month: 2, date: 5 })
    ])
    expect(wrapper.instance().chartTitle).toBe('01.02.2018 – 05.03.2019')
  })
})
