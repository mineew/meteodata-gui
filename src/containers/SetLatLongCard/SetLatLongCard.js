import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'antd'
import { LoadingCard, LatLong, QuitButton } from '../../components'
import { errorOccurred } from '../../notifications'
import { locations } from '../../config'

import gql from 'graphql-tag.macro'
import { Mutation } from 'react-apollo'

export const SET_LAT_LONG = gql`
  mutation SetLatLong ($latitude: Float!, $longitude: Float!) {
    setLatLong(latitude: $latitude, longitude: $longitude)
  }
`

/**
 * @typedef {Object} SetLatLongCardProps
 * @property {function(number, number)} onSetLatLong
 */

/**
 * @augments {React.Component<SetLatLongCardProps>}
 */
export class SetLatLongCard extends PureComponent {
  state = {
    latitude: locations[0].latitude,
    longitude: locations[0].longitude
  }

  render () {
    const { latitude, longitude } = this.state

    return (
      <Mutation
        mutation={SET_LAT_LONG}
        onCompleted={this.handleCompleted}
        onError={errorOccurred}
      >
        {(fireMutation, { loading }) => (
          <LoadingCard
            title="Укажите широту и долготу"
            icon="compass"
            loading={loading}
            loadingTip="Подождите, сохраняются координаты..."
            actions={[
              <Button
                type="primary"
                icon="double-right"
                onClick={() => this.handleClick(fireMutation)}
              >
                Сохранить
              </Button>,
              <QuitButton />
            ]}
          >
            <LatLong
              locations={locations}
              value={[latitude, longitude]}
              onChange={this.handleLatLongChange}
            />
          </LoadingCard>
        )}
      </Mutation>
    )
  }

  handleCompleted = () => {
    const { latitude, longitude } = this.state
    const { onSetLatLong } = this.props
    onSetLatLong(latitude, longitude)
  }

  handleClick = (fireMutation) => {
    const { latitude, longitude } = this.state
    fireMutation({ variables: { latitude, longitude } })
  }

  handleLatLongChange = (latitude, longitude) => {
    this.setState({ latitude, longitude })
  }
}

SetLatLongCard.props = {
  onSetLatLong: PropTypes.func.isRequired
}

export default SetLatLongCard
