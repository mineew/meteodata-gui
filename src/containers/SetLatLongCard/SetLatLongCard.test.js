import React from 'react'
import { mount } from 'enzyme'
import waitForExpect from 'wait-for-expect'
import { MockedProvider } from 'react-apollo/test-utils'
import * as notifications from '../../notifications'
import { locations } from '../../config'
import { SET_LAT_LONG, SetLatLongCard } from './SetLatLongCard'

notifications.errorOccurred = jest.fn()

const normalMocks = [{
  request: {
    query: SET_LAT_LONG,
    variables: {
      latitude: locations[0].latitude,
      longitude: locations[0].longitude
    }
  },
  result: {
    data: {
      setLatLong: true
    }
  }
}]

const errorMocks = [{
  request: {
    query: SET_LAT_LONG,
    variables: {
      latitude: locations[0].latitude,
      longitude: locations[0].longitude
    }
  },
  error: new Error('some error message')
}]

describe('<SetLatLongCard />', () => {
  it('shows the initial state', () => {
    mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <SetLatLongCard onSetLatLong={jest.fn()} />
      </MockedProvider>
    )
  })

  it('shows the loading state', () => {
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <SetLatLongCard onSetLatLong={jest.fn()} />
      </MockedProvider>
    )
    expect(wrapper.html()).not.toMatch('ant-spin-spinning')
    const loadBtn = wrapper.find('.ant-btn-primary')
    loadBtn.simulate('click')
    expect(wrapper.html()).toMatch('ant-spin-spinning')
  })

  it('shows the final state', async () => {
    const onSetLatLong = jest.fn()
    const wrapper = mount(
      <MockedProvider mocks={normalMocks} addTypename={false}>
        <SetLatLongCard onSetLatLong={onSetLatLong} />
      </MockedProvider>
    )
    const { latitude, longitude } = locations[0]
    const card = wrapper.find('SetLatLongCard')
    card.instance().handleLatLongChange(latitude, longitude)
    const loadBtn = wrapper.find('.ant-btn-primary')
    loadBtn.simulate('click')
    await waitForExpect(() => {
      expect(onSetLatLong).toHaveBeenCalledTimes(1)
      expect(onSetLatLong).toHaveBeenLastCalledWith(latitude, longitude)
    })
  })

  it('shows the error state', async () => {
    const wrapper = mount(
      <MockedProvider mocks={errorMocks} addTypename={false}>
        <SetLatLongCard onSetLatLong={jest.fn()} />
      </MockedProvider>
    )
    const loadBtn = wrapper.find('.ant-btn-primary')
    loadBtn.simulate('click')
    await waitForExpect(() => {
      expect(notifications.errorOccurred).toHaveBeenCalledTimes(1)
    })
  })
})
