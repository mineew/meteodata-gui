import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import * as notifications from './notifications'
import App from './App'

jest.useFakeTimers()

notifications.searchComplete = jest.fn()
notifications.invalidPathsDetected = jest.fn()
notifications.closeNotifications = jest.fn()

describe('<App />', () => {
  it('renders `SetRootCard` when the root isn\'t specified', () => {
    const wrapper = shallow(<App />)
    expect(wrapper.find('SetRootCard')).toHaveLength(1)
  })

  it('shows `searchComplete` when the root is specified', () => {
    const wrapper = shallow(<App />)
    wrapper.instance().handleSetRoot({ root: '/some/root' })
    expect(notifications.searchComplete).toHaveBeenCalledTimes(1)
  })

  it('shows `invalidPathsDetected` when there are invalid paths', () => {
    const wrapper = shallow(<App />)
    wrapper.setState({
      root: '/some/root',
      invalidPaths: ['/path/to/some/file']
    })
    jest.runAllTimers()
    expect(notifications.invalidPathsDetected).toHaveBeenCalledTimes(1)
  })

  it('renders `LoadDatabaseCard` when the metrics aren\'t loaded', () => {
    const wrapper = shallow(<App />)
    wrapper.instance().handleSetRoot({ root: '/some/root' })
    expect(wrapper.find('LoadDatabaseCard')).toHaveLength(1)
  })

  it('closes notifications when the metrics are loaded', () => {
    const wrapper = shallow(<App />)
    wrapper.setState({
      root: '/some/root',
      metricsCount: 10
    })
    expect(notifications.closeNotifications).toHaveBeenCalledTimes(1)
  })

  it('shows `SetLatLongCard` when the metrics are loaded', () => {
    const wrapper = shallow(<App />)
    wrapper.instance().handleSetRoot({ root: '/some/root' })
    wrapper.instance().handleDatabaseLoaded({ metricsCount: 10 })
    expect(wrapper.find('SetLatLongCard')).toHaveLength(1)
  })

  it('renders App layout when the location is specified', () => {
    const wrapper = shallow(<App />)
    wrapper.instance().handleSetRoot({ root: '/some/root' })
    wrapper.instance().handleDatabaseLoaded({
      metricsCount: 10,
      minDate: moment(),
      maxDate: moment().add(1, 'year')
    })
    wrapper.instance().handleSetLatLong(1, 2)
    expect(wrapper.find('SetRootCard')).toHaveLength(0)
    expect(wrapper.find('LoadDatabaseCard')).toHaveLength(0)
    expect(wrapper.find('SetLatLongCard')).toHaveLength(0)
    expect(wrapper.find('Tabs')).toHaveLength(1)
    expect(wrapper.find('Tabs').dive().find('Calendar')).toHaveLength(1)
    expect(wrapper.find('Tabs').dive().find('Table')).toHaveLength(1)
    expect(wrapper.find('Tabs').dive().find('AppInfo')).toHaveLength(1)
  })
})
