import React from 'react'
import { shallow } from 'enzyme'
import { notification } from 'antd'
import moment from 'moment'
import humanizeDuration from 'humanize-duration'
import {
  errorOccurred,
  searchComplete,
  invalidPathsDetected,
  fileReadComplete,
  closeNotifications
} from './index'

notification.error = jest.fn()
notification.info = jest.fn()
notification.warning = jest.fn()
notification.success = jest.fn()
notification.close = jest.fn()

beforeEach(() => {
  notification.error.mockClear()
  notification.info.mockClear()
  notification.warning.mockClear()
  notification.success.mockClear()
  notification.close.mockClear()
})

describe('errorOccurred', () => {
  it('renders string errors', () => {
    errorOccurred('an error 1')
    expect(notification.error).toHaveBeenCalledTimes(1)
    expect(notification.error.mock.calls[0][0].description).toBe('an error 1')
  })

  it('renders object errors', () => {
    errorOccurred({ message: 'an error 2' })
    expect(notification.error).toHaveBeenCalledTimes(1)
    expect(notification.error.mock.calls[0][0].description).toBe('an error 2')
  })

  it('renders nested object errors', () => {
    errorOccurred({
      message: 'an error 3',
      inner: {
        message: 'an error 4'
      }
    })
    expect(notification.error).toHaveBeenCalledTimes(1)
    const { description } = notification.error.mock.calls[0][0]
    expect(shallow(description).html()).toBe(shallow(
      <ul className="error-list">
        <li>an error 3</li>
        <li>an error 4</li>
      </ul>
    ).html())
  })

  it('closes previously opened errors', () => {
    errorOccurred('an error 1')
    expect(notification.close).toHaveBeenCalledTimes(1)
    expect(notification.close).toHaveBeenLastCalledWith('errorOccurred')
  })
})

describe('searchComplete', () => {
  it('renders an info message', () => {
    searchComplete(100)
    expect(notification.info).toHaveBeenCalledTimes(1)
  })

  it('shows number of files', () => {
    searchComplete(123)
    const { description } = notification.info.mock.calls[0][0]
    expect(shallow(description).html()).toMatch('123')
  })

  it('closes previously opened errors', () => {
    searchComplete(500)
    expect(notification.close).toHaveBeenCalledTimes(1)
    expect(notification.close).toHaveBeenLastCalledWith('errorOccurred')
  })
})

describe('invalidPathsDetected', () => {
  it('renders a warning message', () => {
    invalidPathsDetected([])
    expect(notification.warning).toHaveBeenCalledTimes(1)
  })

  it('shows invalid paths basenames', () => {
    invalidPathsDetected(['/path/to/some/file'])
    const { description } = notification.warning.mock.calls[0][0]
    expect(shallow(description).html()).toMatch('file')
  })

  it('shows only limited list of invalid paths', () => {
    invalidPathsDetected([
      '/path/to/some/file1',
      '/path/to/some/file2',
      '/path/to/some/file3'
    ], 2)
    const { description } = notification.warning.mock.calls[0][0]
    expect(shallow(description).html()).toMatch('еще 1')
  })
})

describe('fileReadComplete', () => {
  let opts = {
    filesCount: 1234561,
    metricsCount: 1234562,
    missedMetricsCount: 1234563,
    brokenMetricsCount: 1234564,
    minDate: moment({ year: 2017, month: 0, day: 1 }),
    maxDate: moment({ year: 2018, month: 11, day: 31 })
  }

  it('renders a success message', () => {
    fileReadComplete(opts)
    expect(notification.success).toHaveBeenCalledTimes(1)
  })

  it('shows all necessary information', () => {
    fileReadComplete(opts)
    const { description } = notification.success.mock.calls[0][0]
    const html = shallow(description).html()
    expect(html).toMatch(opts.filesCount.toLocaleString('ru-RU'))
    expect(html).toMatch(opts.metricsCount.toLocaleString('ru-RU'))
    expect(html).toMatch(opts.missedMetricsCount.toLocaleString('ru-RU'))
    expect(html).toMatch(opts.brokenMetricsCount.toLocaleString('ru-RU'))
    const { minDate, maxDate } = opts
    const minDatef = minDate.format('DD.MM.YY HH:mm')
    const maxDatef = maxDate.format('DD.MM.YY HH:mm')
    expect(html).toMatch(`${minDatef} - ${maxDatef}`)
    const duration = maxDate.diff(minDate)
    expect(html).toMatch(humanizeDuration(duration, { language: 'ru' }))
  })

  it('doesn\'t show zeros', () => {
    fileReadComplete({ ...opts, missedMetricsCount: 0, brokenMetricsCount: 0 })
    const { description } = notification.success.mock.calls[0][0]
    const html = shallow(description).html()
    expect(html).not.toMatch('пропущенных')
    expect(html).not.toMatch('битых')
  })

  it('closes previously opened errors and infos', () => {
    fileReadComplete(opts)
    expect(notification.close).toHaveBeenCalledTimes(2)
    expect(notification.close.mock.calls[0][0]).toBe('errorOccurred')
    expect(notification.close.mock.calls[1][0]).toBe('searchComplete')
  })
})

describe('closeNotifications', () => {
  it('closes specific notifications', () => {
    closeNotifications()
    expect(notification.close).toHaveBeenCalledTimes(2)
    expect(notification.close.mock.calls[0][0]).toBe('errorOccurred')
    expect(notification.close.mock.calls[1][0]).toBe('searchComplete')
  })
})
