import React from 'react'
import { notification, Icon, Popover, Input } from 'antd'
import traverse from 'traverse'
import humanizeDuration from 'humanize-duration'
import './index.css'

const pt = window.require('electron').remote.require('path')

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @param {string|object} error
 */
export function errorOccurred (error) {
  notification.close('errorOccurred')
  notification.error({
    key: 'errorOccurred',
    duration: null,
    message: 'Произошла ошибка',
    description: _renderMessages(error)
  })
}

/**
 * @param {number} files
 */
export function searchComplete (files) {
  const filesf = files.toLocaleString('ru-RU')
  const description = (
    <span>
      <Icon type="file" /> <b>{filesf}</b> файлов найдено
    </span>
  )
  notification.close('errorOccurred')
  notification.info({
    key: 'searchComplete',
    duration: null,
    message: 'Поиск завершен',
    description
  })
}

/**
 * @param {Array.<string>} paths
 * @param {number} showLength
 */
export function invalidPathsDetected (paths, showLength = 5) {
  const remain = paths.length - showLength
  const remainf = remain > 0 ? remain.toLocaleString('ru-RU') : ''
  const description = (
    <div>
      {paths.slice(0, showLength).map((path) => {
        const basename = pt.basename(path)
        const input = <Input size="small" defaultValue={path} />
        return (
          <div key={path} className="invalid-path">
            <Popover content={input}>
              <code>{basename}</code>
            </Popover>
          </div>
        )
      })}
      {remainf && (
        <span>
          <Icon type="ellipsis" /> еще {remainf}
        </span>
      )}
    </div>
  )
  notification.warning({
    duration: null,
    message: 'Обнаружены неверные файлы',
    description
  })
}

/**
 * @typedef {Object} FileReadCompleteOpts
 * @property {number} filesCount
 * @property {number} metricsCount
 * @property {number} missedMetricsCount
 * @property {number} brokenMetricsCount
 * @property {Moment} minDate
 * @property {Moment} maxDate
 */

/**
 * @param {FileReadCompleteOpts} opts
 */
export function fileReadComplete (opts) {
  const filesf = opts.filesCount.toLocaleString('ru-RU')
  const metricsf = opts.metricsCount.toLocaleString('ru-RU')
  const missedf = opts.missedMetricsCount
    ? opts.missedMetricsCount.toLocaleString('ru-RU')
    : ''
  const brokenf = opts.brokenMetricsCount
    ? opts.brokenMetricsCount.toLocaleString('ru-RU')
    : ''
  const minDatef = opts.minDate.format('DD.MM.YY HH:mm')
  const maxDatef = opts.maxDate.format('DD.MM.YY HH:mm')
  const duration = opts.maxDate.diff(opts.minDate)
  const durationf = humanizeDuration(duration, {
    language: 'ru'
  })
  const description = (
    <div className="file-read-complete-success">
      <div>
        <Icon type="file" /> <b>{filesf}</b> файлов загружено
      </div>
      <div>
        <Icon type="bars" /> <b>{metricsf}</b> всего измерений
      </div>
      {missedf && (
        <div>
          <Icon type="stop" /> <b>{missedf}</b> пропущенных измерений
        </div>
      )}
      {brokenf && (
        <div>
          <Icon type="warning" /> <b>{brokenf}</b> "битых" измерений
        </div>
      )}
      <div>
        <Icon type="calendar" /> {minDatef} - {maxDatef}
      </div>
      <hr />
      <Icon type="clock-circle" /> {durationf}
    </div>
  )
  notification.close('errorOccurred')
  notification.close('searchComplete')
  notification.success({
    duration: null,
    placement: 'bottomRight',
    message: 'Чтение файлов завершено',
    description
  })
}

export function closeNotifications () {
  notification.close('errorOccurred')
  notification.close('searchComplete')
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {object} obj
 * @returns {Array.<string>}
 */
function _getMessages (obj) {
  const messages = []
  traverse(obj).forEach(function (value) {
    if (this.key === 'message' && !value.includes('Network error')) {
      messages.push(value)
    }
  })
  return messages
}

/**
 * @private
 * @param {string|object} error
 * @returns {string|React.ReactNode}
 */
function _renderMessages (error) {
  if (typeof error === 'string') {
    return error
  }
  const messages = _getMessages(error)
  if (messages.length === 1) {
    return messages[0]
  }
  return (
    <ul className="error-list">
      {messages.map((msg) => (
        <li key={msg}>{msg}</li>
      ))}
    </ul>
  )
}
