import React from 'react'
import { mount } from 'enzyme'
import ErrorBoundary from './ErrorBoundary'

const SomeBuggyComponent = () => {
  throw new Error('It does not look good')
}

describe('<ErrorBoundary />', () => {
  it('renders an error message', () => {
    const wrapper = mount(
      <ErrorBoundary>
        <p>Everything seems fine.</p>
      </ErrorBoundary>
    )
    wrapper.setState({ error: new Error('No, that is really bad.') })
    expect(wrapper.html()).toMatch('No, that is really bad.')
  })

  it('catches errors', () => {
    // mute jsdom's console
    jest.spyOn(window._virtualConsole, 'emit').mockImplementation(() => false)
    jest.spyOn(ErrorBoundary, 'getDerivedStateFromError')
    mount(
      <ErrorBoundary>
        <SomeBuggyComponent />
      </ErrorBoundary>
    )
    expect(ErrorBoundary.getDerivedStateFromError).toHaveBeenCalled()
  })
})
