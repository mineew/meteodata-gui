import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import moment from 'moment'
import 'moment/locale/ru'
moment.locale('ru')

configure({ adapter: new Adapter() })

const pathMock = {
  basename (path) {
    return path.split('/').slice(-1)[0]
  }
}

const electronMock = {
  remote: {
    dialog: {
      showOpenDialog: jest.fn((opts, onOpen) => {
        onOpen(electronMock.remote.dialog.showOpenDialogPaths)
      }),
      showOpenDialogPaths: [],
      showSaveDialog: jest.fn((opts, onOpen) => {
        onOpen(electronMock.remote.dialog.showSaveDialogPath)
      }),
      showSaveDialogPath: ''
    },
    app: {
      quit: jest.fn()
    },
    require (name) {
      if (name === 'path') {
        return pathMock
      }
    }
  }
}

window.require = (name) => {
  if (name === 'electron') {
    return electronMock
  }
}
