const locations = [{
  name: 'Пинега',
  latitude: 64.697539,
  longitude: 43.391557
}]

const metricsNames = [
  ['Скорость ветра', 'windSpeed'],
  ['Направление ветра', 'windDirection'],
  ['Температура улицы', 'outsideTemperature'],
  ['Температура помещения', 'insideTemperature'],
  ['Влажность улицы', 'outsideHumidity'],
  ['Влажность помещения', 'insideHumidity'],
  ['Точка росы', 'outsideDewpoint'],
  ['Давление', 'pressure'],
  ['Осадки', 'rain'],
  ['Температура порыва ветра', 'windChill']
]

const serverPort = 5349

module.exports = {
  reactDevUrl: 'http://localhost:3000',
  reactProdFile: '../build/index.html',
  serverPort,
  serverUrl: `http://localhost:${serverPort}/graphql`,
  serverTimeout: 25 * 1000,
  locations,
  metricsNames
}
