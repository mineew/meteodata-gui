import React from 'react'
import { mount } from 'enzyme'
import SaveExcelButton from './SaveExcelButton'
const { dialog } = window.require('electron').remote

afterEach(() => {
  dialog.showSaveDialog.mockClear()
})

describe('<SaveExcelButton />', () => {
  it('can take `onSelectPath` prop', () => {
    dialog.showSaveDialogPath = '/some/path'
    const onSelectPath = jest.fn()
    const wrapper = mount(<SaveExcelButton onSelectPath={onSelectPath} />)
    wrapper.find('SaveButton').simulate('click')
    expect(onSelectPath).toHaveBeenCalledTimes(1)
  })

  it('can take `filename` prop', () => {
    const wrapper = mount(
      <SaveExcelButton filename="file.xls" onSelectPath={jest.fn()} />
    )
    wrapper.find('SaveButton').simulate('click')
    const dialogOptions = dialog.showSaveDialog.mock.calls[0][0]
    expect(dialogOptions.defaultPath).toBe('file.xls')
  })

  it('can take `loading` prop', () => {
    const wrapper = mount(
      <SaveExcelButton loading={true} onSelectPath={jest.fn()} />
    )
    expect(wrapper.html()).toMatch('ant-btn-loading')
  })
})
