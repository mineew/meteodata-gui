import React from 'react'
import PropTypes from 'prop-types'
import SaveButton from '../SaveButton/SaveButton'

/**
 * @typedef {Object} SaveExcelButtonProps
 * @property {function(string)} onSelectPath
 * @property {string} [filename]
 * @property {boolean} [loading]
 */

/**
 * @param {SaveExcelButtonProps} props
 * @returns {React.Component<SaveExcelButtonProps>}
 */
function SaveExcelButton (props) {
  const { filename, loading, onSelectPath } = props

  const xlsFilesFilter = {
    name: 'Файлы Excel',
    extensions: ['xlsx', 'xls']
  }

  return (
    <SaveButton
      title="Excel"
      icon="file-excel"
      filename={filename}
      filters={[xlsFilesFilter]}
      loading={loading}
      onSelectPath={onSelectPath}
    />
  )
}

SaveExcelButton.defaultProps = {
  loading: false
}

SaveExcelButton.propTypes = {
  onSelectPath: PropTypes.func.isRequired,
  filename: PropTypes.string,
  loading: PropTypes.bool
}

export default SaveExcelButton
