import React from 'react'
import { mount } from 'enzyme'
import moment from 'moment'
import AppInfo from './AppInfo'

const props = {
  root: '/some/root',
  filesCount: 1234,
  metricsCount: 45681,
  missedMetricsCount: 2311,
  brokenMetricsCount: 1111,
  minDate: moment({ year: 2018, month: 4, day: 1, hours: 0, minutes: 0 }),
  maxDate: moment({ year: 2018, month: 9, day: 30, hours: 23, minutes: 50 }),
  latitude: 64.5,
  longitude: 44.5
}

const html = mount(<AppInfo {...props} />).html()
const htmlWithZeros = mount(
  <AppInfo
    {...props}
    missedMetricsCount={0}
    brokenMetricsCount={0}
  />
).html()

describe('<AppInfo />', () => {
  it('shows the root', () => {
    expect(html).toMatch(props.root)
  })

  it('shows the files count', () => {
    expect(html).toMatch(
      props.filesCount.toLocaleString('ru-RU')
    )
  })

  it('shows the metrics count', () => {
    expect(html).toMatch(
      props.metricsCount.toLocaleString('ru-RU')
    )
  })

  it('shows the missed metrics count', () => {
    expect(html).toMatch(
      props.missedMetricsCount.toLocaleString('ru-RU')
    )
  })

  it('shows the broken metrics count', () => {
    expect(html).toMatch(
      props.brokenMetricsCount.toLocaleString('ru-RU')
    )
  })

  it('shows dashes if there are zeros', () => {
    expect(htmlWithZeros).toMatch('<span>-</span>')
  })

  it('shows the date range', () => {
    expect(html).toMatch(props.minDate.format('DD.MM.YY HH:mm'))
    expect(html).toMatch(props.maxDate.format('DD.MM.YY HH:mm'))
  })

  it('shows the duration', () => {
    expect(html).toMatch('6 месяцев, 8 часов, 50 минут')
  })

  it('shows the coords', () => {
    expect(html).toMatch(props.latitude.toString())
    expect(html).toMatch(props.longitude.toString())
  })
})
