import React from 'react'
import PropTypes from 'prop-types'
import { List, Icon } from 'antd'
import Center from '../Center/Center'
import humanizeDuration from 'humanize-duration'
import './AppInfo.css'

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {Object} AppInfoProps
 * @property {srting} root
 * @property {number} filesCount
 * @property {number} metricsCount
 * @property {number} missedMetricsCount
 * @property {number} brokenMetricsCount
 * @property {Moment} minDate
 * @property {Moment} maxDate
 * @property {number} latitude
 * @property {number} longitude
 */

/**
 * @param {AppInfoProps} props
 * @returns {React.Component<AppInfoProps>}
 */
function AppInfo (props) {
  const {
    root,
    filesCount,
    metricsCount,
    missedMetricsCount,
    brokenMetricsCount,
    minDate,
    maxDate,
    latitude,
    longitude
  } = props

  const filesf = filesCount.toLocaleString('ru-RU')
  const metricsf = metricsCount.toLocaleString('ru-RU')
  const missedf = missedMetricsCount
    ? missedMetricsCount.toLocaleString('ru-RU')
    : '-'
  const brokenf = brokenMetricsCount
    ? brokenMetricsCount.toLocaleString('ru-RU')
    : '-'
  const minDatef = minDate.format('DD.MM.YY HH:mm')
  const maxDatef = maxDate.format('DD.MM.YY HH:mm')
  const duration = maxDate.diff(minDate)
  const durationf = humanizeDuration(duration, {
    language: 'ru'
  })

  return (
    <div className="AppInfo">
      <Center>
        <List
          bordered={true}
          size="small"
        >
          <List.Item>
            <List.Item.Meta
              title={<div><Icon type="folder-open" /> Папка с файлами</div>}
            />
            <span>{root}</span>
          </List.Item>
          <List.Item>
            <List.Item.Meta
              title={<div><Icon type="file" /> Загружено файлов</div>}
            />
            <span>{filesf}</span>
          </List.Item>
          <List.Item>
            <List.Item.Meta
              title={<div><Icon type="bars" /> Загружено измерений</div>}
            />
            <span>{metricsf}</span>
          </List.Item>
          <List.Item>
            <List.Item.Meta
              title={<div><Icon type="stop" /> Пропущенных измерений</div>}
            />
            <span>{missedf}</span>
          </List.Item>
          <List.Item>
            <List.Item.Meta
              title={<div><Icon type="warning" /> "Битых" измерений</div>}
            />
            <span>{brokenf}</span>
          </List.Item>
          <List.Item>
            <List.Item.Meta
              title={<div><Icon type="calendar" /> Период измерений</div>}
            />
            <span>{minDatef} – {maxDatef}</span>
          </List.Item>
          <List.Item>
            <List.Item.Meta
              title={
                <div><Icon type="clock-circle" /> Длительность измерений</div>
              }
            />
            <span>{durationf}</span>
          </List.Item>
          <List.Item>
            <List.Item.Meta
              title={<div><Icon type="compass" /> Координаты</div>}
            />
            <span>{latitude}, {longitude}</span>
          </List.Item>
        </List>
      </Center>
    </div>
  )
}

AppInfo.propTypes = {
  root: PropTypes.string.isRequired,
  filesCount: PropTypes.number.isRequired,
  metricsCount: PropTypes.number.isRequired,
  missedMetricsCount: PropTypes.number.isRequired,
  brokenMetricsCount: PropTypes.number.isRequired,
  minDate: PropTypes.object.isRequired,
  maxDate: PropTypes.object.isRequired,
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired
}

export default AppInfo
