import React from 'react'
import PropTypes from 'prop-types'
import { Card, Icon, Spin } from 'antd'
import './LoadingCard.css'

/**
 * @typedef {Object} LoadingCardProps
 * @property {string} title
 * @property {string} icon
 * @property {React.ReactNode} children
 * @property {boolean} [loading]
 * @property {string} [loadingTip]
 * @property {Array.<React.ReactNode>} [actions]
 */

/**
 * @param {LoadingCardProps} props
 * @returns {React.Component.<LoadingCardProps>}
 */
function LoadingCard (props) {
  const {
    title,
    icon: iconType,
    children: description,
    loading,
    loadingTip,
    actions
  } = props
  const icon = <Icon type={iconType} />

  return (
    <div className="LoadingCard">
      <Spin spinning={loading} tip={loadingTip}>
        <Card actions={actions}>
          <Card.Meta
            title={title}
            description={description}
            avatar={icon}
          />
        </Card>
      </Spin>
    </div>
  )
}

LoadingCard.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  loading: PropTypes.bool,
  loadingTip: PropTypes.string,
  actions: PropTypes.arrayOf(PropTypes.node)
}

export default LoadingCard
