import React from 'react'
import { mount } from 'enzyme'
import LoadingCard from './LoadingCard'

describe('<LoadingCard />', () => {
  it('takes required props', () => {
    const wrapper = mount(
      <LoadingCard title="title" icon="user">
        <p>children</p>
      </LoadingCard>
    )
    expect(wrapper.text()).toMatch('children')
    expect(wrapper.text()).toMatch('title')
    expect(wrapper.html()).toMatch('anticon-user')
  })

  it('shows loading state', () => {
    const wrapperLoading = mount(
      <LoadingCard title="title" icon="user" loading={true}>
        <p>children</p>
      </LoadingCard>
    )
    expect(wrapperLoading.html()).toMatch('ant-spin-spinning')
    const wrapperLoadingTip = mount(
      <LoadingCard
        title="title"
        icon="user"
        loading={true}
        loadingTip="loading tip"
      >
        <p>children</p>
      </LoadingCard>
    )
    expect(wrapperLoadingTip.text()).toMatch('loading tip')
    const wrapperNotLoading = mount(
      <LoadingCard title="title" icon="user" loading={false}>
        <p>children</p>
      </LoadingCard>
    )
    expect(wrapperNotLoading.html()).not.toMatch('ant-spin-spinning')
  })

  it('renders actions', () => {
    const wrapper = mount(
      <LoadingCard
        title="title"
        icon="user"
        actions={[
          <p className="action">Action 1</p>,
          <p className="action">Action 2</p>
        ]}
      >
        <p>children</p>
      </LoadingCard>
    )
    expect(wrapper.find('.action')).toHaveLength(2)
    expect(wrapper.find('.action').at(0).text()).toBe('Action 1')
    expect(wrapper.find('.action').at(1).text()).toBe('Action 2')
  })
})
