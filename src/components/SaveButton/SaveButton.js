import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'antd'
const { dialog } = window.require('electron').remote

/**
 * @typedef {Object} FileFilter
 * @property {string} name
 * @property {Array.<string>} extensions
 */

/**
 * @typedef {Object} SaveButtonProps
 * @property {function(string)} onSelectPath
 * @property {string} [title]
 * @property {string} [icon]
 * @property {string} [filename]
 * @property {Array.<FileFilter>} [filters]
 * @property {boolean} [loading]
 */

/**
 * @param {SaveButtonProps} props
 * @returns {React.Component<SaveButtonProps>}
 */
function SaveButton (props) {
  const {
    title,
    icon,
    filename: defaultPath,
    filters,
    loading,
    onSelectPath
  } = props

  const allFilesFilter = {
    name: 'Все файлы',
    extensions: ['*']
  }

  const onClick = () => {
    const dialogOptions = {
      defaultPath,
      filters: [allFilesFilter, ...filters]
    }
    dialog.showSaveDialog(dialogOptions, (path) => {
      if (path) {
        onSelectPath(path)
      }
    })
  }

  return (
    <Button
      className="SaveButton"
      type="primary"
      icon={icon}
      loading={loading}
      onClick={onClick}
    >
      {title}
    </Button>
  )
}

SaveButton.defaultProps = {
  title: 'Сохранить',
  icon: 'save',
  filters: [],
  loading: false
}

SaveButton.propTypes = {
  onSelectPath: PropTypes.func.isRequired,
  title: PropTypes.string,
  icon: PropTypes.string,
  filename: PropTypes.string,
  filters: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    extensions: PropTypes.arrayOf(PropTypes.string).isRequired
  })),
  loading: PropTypes.bool
}

export default SaveButton
