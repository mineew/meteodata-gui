import React from 'react'
import { shallow } from 'enzyme'
import SaveButton from './SaveButton'
const { dialog } = window.require('electron').remote

const allFilesFilter = {
  name: 'Все файлы',
  extensions: ['*']
}

const jsFilesFilter = {
  name: 'JavaScript files',
  extensions: ['*.js', '*.jsx']
}

afterEach(() => {
  dialog.showSaveDialog.mockClear()
})

describe('<SaveButton />', () => {
  it('calls `showSaveDialog` on click', () => {
    const wrapper = shallow(<SaveButton onSelectPath={jest.fn()} />)
    wrapper.simulate('click')
    expect(dialog.showSaveDialog).toHaveBeenCalledTimes(1)
  })

  it('selects the path specified by the user', () => {
    dialog.showSaveDialogPath = '/some/path'
    const onSelectPath = jest.fn()
    const wrapper = shallow(<SaveButton onSelectPath={onSelectPath} />)
    wrapper.simulate('click')
    expect(onSelectPath).toHaveBeenCalledTimes(1)
    expect(onSelectPath).toHaveBeenLastCalledWith('/some/path')
  })

  it('doesn\'t select anything if the user did not specify a path', () => {
    dialog.showSaveDialogPath = ''
    const onSelectPath = jest.fn()
    const wrapper = shallow(<SaveButton onSelectPath={onSelectPath} />)
    wrapper.simulate('click')
    expect(onSelectPath).not.toHaveBeenCalled()
  })

  it('renders title', () => {
    const wrapper = shallow(
      <SaveButton title="SaveButtonTitle" onSelectPath={jest.fn()} />
    )
    expect(wrapper.html()).toMatch('SaveButtonTitle')
  })

  it('renders icon', () => {
    const wrapper = shallow(
      <SaveButton icon="user" onSelectPath={jest.fn()} />
    )
    expect(wrapper.html()).toMatch('anticon-user')
  })

  it('can specify a default filename', () => {
    const wrapper = shallow(
      <SaveButton filename="file.js" onSelectPath={jest.fn()} />
    )
    wrapper.simulate('click')
    const dialogOptions = dialog.showSaveDialog.mock.calls[0][0]
    expect(dialogOptions.defaultPath).toBe('file.js')
  })

  it('shows "all files" filter', () => {
    const wrapper = shallow(
      <SaveButton filename="file.js" onSelectPath={jest.fn()} />
    )
    wrapper.simulate('click')
    const dialogOptions = dialog.showSaveDialog.mock.calls[0][0]
    expect(dialogOptions.filters).toEqual([allFilesFilter])
  })

  it('can specify additional file filters', () => {
    const wrapper = shallow(
      <SaveButton filters={[jsFilesFilter]} onSelectPath={jest.fn()} />
    )
    wrapper.simulate('click')
    const dialogOptions = dialog.showSaveDialog.mock.calls[0][0]
    expect(dialogOptions.filters).toEqual([allFilesFilter, jsFilesFilter])
  })

  it('shows loading state', () => {
    const wrapper = shallow(
      <SaveButton loading={true} onSelectPath={jest.fn()} />
    )
    expect(wrapper.html()).toMatch('ant-btn-loading')
  })
})
