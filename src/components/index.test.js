import * as componentsModule from './index'

describe('`components` module', () => {
  it('exports as expected', () => {
    const contents = Object.keys(componentsModule)
    contents.sort()
    expect(contents).toEqual([
      'AppInfo',
      'CalendarCell',
      'Center',
      'Chart',
      'Checkboxes',
      'DrawerButton',
      'LatLong',
      'LoadingCard',
      'Menu',
      'OpenButton',
      'QuitButton',
      'SaveButton',
      'SaveExcelButton',
      'Tabs'
    ])
  })
})
