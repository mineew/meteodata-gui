import React from 'react'
import { shallow } from 'enzyme'
import QuitButton from './QuitButton'
const { app } = window.require('electron').remote

describe('<QuitButton />', () => {
  it('closes the app', () => {
    const wrapper = shallow(<QuitButton />)
    wrapper.simulate('click')
    expect(app.quit).toHaveBeenCalledTimes(1)
  })
})
