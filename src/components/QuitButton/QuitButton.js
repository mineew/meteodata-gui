import React from 'react'
import { Button } from 'antd'
const { app } = window.require('electron').remote

/**
 * @returns {React.Component}
 */
function QuitButton () {
  const onClick = () => {
    app.quit()
  }

  return (
    <Button
      type="danger"
      icon="close-circle"
      onClick={onClick}
    >
      Выход
    </Button>
  )
}

export default QuitButton
