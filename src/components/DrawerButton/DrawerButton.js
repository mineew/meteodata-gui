import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Drawer } from 'antd'
import './DrawerButton.css'

/**
 * @typedef {Object} DrawerButtonProps
 * @property {string} title
 * @property {React.ReactNode} children
 * @property {string} [icon]
 * @property {string} [drawerTitle]
 * @property {string} [drawerPlacement]
 * @property {number} [drawerWidth]
 * @property {number} [drawerHeight]
 */

/**
 * @augments {React.Component<DrawerButtonProps>}
 */
class DrawerButton extends PureComponent {
  state = {
    visible: false
  }

  render () {
    const {
      title,
      children,
      icon,
      drawerTitle,
      drawerPlacement,
      drawerWidth,
      drawerHeight
    } = this.props
    const { visible } = this.state

    return (
      <Fragment>
        <Button
          className="DrawerButton"
          icon={icon}
          disabled={visible}
          onClick={this.handleClick}
        >
          {title}
        </Button>
        <Drawer
          className="DrawerButton-Drawer"
          title={drawerTitle}
          visible={visible}
          placement={drawerPlacement}
          width={drawerWidth}
          height={drawerHeight}
          onClose={this.handleClose}
          closable={true}
          destroyOnClose={true}
        >
          {children}
        </Drawer>
      </Fragment>
    )
  }

  handleClick = () => {
    this.setState({ visible: true })
  }

  handleClose = () => {
    this.setState({ visible: false })
  }
}

DrawerButton.defaultProps = {
  drawerPlacement: 'bottom',
  drawerHeight: 400
}

DrawerButton.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  icon: PropTypes.string,
  drawerTitle: PropTypes.string,
  drawerPlacement: PropTypes.oneOf(['top', 'right', 'bottom', 'left']),
  drawerWidth: PropTypes.number,
  drawerHeight: PropTypes.number
}

export default DrawerButton
