import React from 'react'
import { mount } from 'enzyme'
import DrawerButton from './DrawerButton'

/**
 * @typedef {import('enzyme').CommonWrapper} CommonWrapper
 */

/**
 * @type {CommonWrapper}
 */
let wrapper = null

beforeEach(() => {
  wrapper = mount(
    <DrawerButton title="Button Title" icon="plus">
      <p>Drawer Content</p>
    </DrawerButton>
  )
})

describe('<DrawerButton />', () => {
  it('renders title', () => {
    expect(wrapper.html()).toMatch('Button Title')
  })

  it('renders icon', () => {
    expect(wrapper.html()).toMatch('anticon-plus')
  })

  it('opens drawer on click', () => {
    wrapper.find('button').simulate('click')
    expect(document.documentElement.innerHTML).toMatch('ant-drawer')
  })

  it('renders children', () => {
    wrapper.find('button').simulate('click')
    expect(document.documentElement.innerHTML).toMatch('Drawer Content')
  })

  it('closes drawer', () => {
    wrapper.find('button').simulate('click')
    expect(wrapper.state('visible')).toBeTruthy()
    wrapper.instance().handleClose()
    expect(wrapper.state('visible')).toBeFalsy()
  })
})
