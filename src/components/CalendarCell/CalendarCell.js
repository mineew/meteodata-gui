import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import './CalendarCell.css'

import { parseMetricsData } from 'meteodata-backend'

import iconFull from './images/full.png'
import iconLight from './images/light.png'
import iconDark from './images/dark.png'
import iconWarning from './images/warning.png'
import iconError from './images/error.png'

/**
 * @typedef {Object} CalendarCellProps
 * @property {object|boolean} data
 * @property {string} metricsName
 * @property {Array.<string>} [show]
 */

/**
 * @param {CalendarCellProps} props
 * @returns {React.Component<CalendarCellProps>}
 */
function CalendarCell (props) {
  const { data, metricsName, show } = props

  if (!data) {
    return ''
  }

  const fullTime = parseMetricsData(data.fullTime)[metricsName]
  const lightTime = parseMetricsData(data.lightTime)[metricsName]
  const darkTime = parseMetricsData(data.darkTime)[metricsName]
  const missed = data.missedCount
  const broken = data.brokenCount

  return (
    <div className="CalendarCell">
      <div className="CalendarCell-row">
        <img src={iconFull} alt="Сутки" />
        {_displayValue(fullTime, show, metricsName)}
      </div>
      <div className="CalendarCell-row">
        <img src={iconLight} alt="Светлое время суток" />
        {_displayValue(lightTime, show, metricsName)}
      </div>
      <div className="CalendarCell-row">
        <img src={iconDark} alt="Темное время суток" />
        {_displayValue(darkTime, show, metricsName)}
      </div>
      {Boolean(missed || broken) && (
        <div className="CalendarCell-row CalendarCell-additional">
          {Boolean(missed) && (
            <Fragment>
              <img src={iconWarning} alt="Пропущенные измерения" />
              <span className="CalendarCell-missed">{missed}</span>
            </Fragment>
          )}
          {Boolean(broken) && (
            <Fragment>
              <img src={iconError} alt="Битые измерения" />
              <span className="CalendarCell-broken">{broken}</span>
            </Fragment>
          )}
        </div>
      )}
    </div>
  )
}

CalendarCell.defaultProps = {
  show: ['mean']
}

CalendarCell.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]).isRequired,
  metricsName: PropTypes.string.isRequired,
  show: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default CalendarCell

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {number|null} n
 * @param {string} metricsName
 * @returns {string}
 */
function _formatNumber (n, metricsName) {
  const pad = metricsName === 'pressure' ? 7 : 6
  return n !== null
    ? n.toFixed(2).padStart(pad)
    : '-'.padStart(pad)
}

/**
 * @private
 * @param {number|{min:number, mean:number, max:number}|null} v
 * @param {Array.<string>} show
 * @param {string} metricsName
 * @returns {React.ReactNode}
 */
function _displayValue (v, show, metricsName) {
  if (typeof v === 'number' || v === null) {
    return (
      <span className="CalendarCell-mean">
        {_formatNumber(v, metricsName)}
      </span>
    )
  }
  const { min, mean, max } = v
  return (
    <Fragment>
      {show.includes('min') && (
        <span className="CalendarCell-min">
          {_formatNumber(min, metricsName)}
        </span>
      )}
      {show.includes('mean') && (
        <span className="CalendarCell-mean">
          {_formatNumber(mean, metricsName)}
        </span>
      )}
      {show.includes('max') && (
        <span className="CalendarCell-max">
          {_formatNumber(max, metricsName)}
        </span>
      )}
    </Fragment>
  )
}
