import React from 'react'
import { mount } from 'enzyme'
import { createEmptyMetricsData } from 'meteodata-backend'
import { metricsNames } from '../../config'
import CalendarCell from './CalendarCell'

const data = {
  lightTime: createEmptyMetricsData(),
  darkTime: createEmptyMetricsData(),
  fullTime: createEmptyMetricsData(),
  missedCount: 10,
  brokenCount: 3
}

data.lightTime[0] = 11
data.lightTime[1] = 12
data.lightTime[2] = 13

data.darkTime[0] = 21
data.darkTime[1] = 22
data.darkTime[2] = 23

data.fullTime[0] = 31
data.fullTime[1] = 32
data.fullTime[2] = 33

const metricsName = metricsNames[0][1]
const numberMetricsName = 'rain'

const testPadMetricsName = 'pressure'

describe('<CalendarCell />', () => {
  it('renders nothing if there is no data', () => {
    const wrapper = mount(<CalendarCell data={false} metricsName={''} />)
    expect(wrapper.html()).toBe(null)
  })

  it('renders missed metrics count', () => {
    const wrapper = mount(
      <CalendarCell data={data} metricsName={metricsName} />
    )
    expect(wrapper.find('.CalendarCell-missed').text()).toBe(
      data.missedCount.toString()
    )
  })

  it('renders broken metrics count', () => {
    const wrapper = mount(
      <CalendarCell data={data} metricsName={metricsName} />
    )
    expect(wrapper.find('.CalendarCell-broken').text()).toBe(
      data.brokenCount.toString()
    )
  })

  it('doesn\'t render metrics counts if they are zeros', () => {
    const wrapper = mount(
      <CalendarCell
        data={{ ...data, missedCount: 0, brokenCount: 0 }}
        metricsName={metricsName}
      />
    )
    expect(wrapper.find('.CalendarCell-missed')).toHaveLength(0)
    expect(wrapper.find('.CalendarCell-broken')).toHaveLength(0)
  })

  it('by default renders only means', () => {
    const wrapper = mount(
      <CalendarCell data={data} metricsName={metricsName} />
    )
    const html = wrapper.html()
    expect(html).toMatch(data.lightTime[0].toString())
    expect(html).not.toMatch(data.lightTime[1].toString())
    expect(html).not.toMatch(data.lightTime[2].toString())
    expect(html).toMatch(data.darkTime[0].toString())
    expect(html).not.toMatch(data.darkTime[1].toString())
    expect(html).not.toMatch(data.darkTime[2].toString())
    expect(html).toMatch(data.fullTime[0].toString())
    expect(html).not.toMatch(data.fullTime[1].toString())
    expect(html).not.toMatch(data.fullTime[2].toString())
  })

  it('can render all possible values', () => {
    const wrapper = mount(
      <CalendarCell
        data={data}
        metricsName={metricsName}
        show={['min', 'mean', 'max']}
      />
    )
    const html = wrapper.html()
    expect(html).toMatch(data.lightTime[0].toString())
    expect(html).toMatch(data.lightTime[1].toString())
    expect(html).toMatch(data.lightTime[2].toString())
    expect(html).toMatch(data.darkTime[0].toString())
    expect(html).toMatch(data.darkTime[1].toString())
    expect(html).toMatch(data.darkTime[2].toString())
    expect(html).toMatch(data.fullTime[0].toString())
    expect(html).toMatch(data.fullTime[1].toString())
    expect(html).toMatch(data.fullTime[2].toString())
  })

  it('can render numbers', () => {
    const wrapper = mount(
      <CalendarCell
        data={{
          ...data,
          fullTime: createEmptyMetricsData().fill(123)
        }}
        metricsName={numberMetricsName}
        show={['min', 'mean', 'max']}
      />
    )
    expect(wrapper.find('.CalendarCell-min')).toHaveLength(0)
    expect(wrapper.find('.CalendarCell-max')).toHaveLength(0)
    expect(wrapper.find('.CalendarCell-mean').at(0).text()).toMatch('123')
  })

  it('can render nulls', () => {
    const wrapper = mount(
      <CalendarCell
        data={data}
        metricsName={numberMetricsName}
        show={['min', 'mean', 'max']}
      />
    )
    expect(wrapper.find('.CalendarCell-mean').at(0).text()).toMatch('-')
  })

  it('pads values correctly', () => {
    const wrapperSmallPad = mount(
      <CalendarCell
        data={data}
        metricsName={numberMetricsName}
        show={['min', 'mean', 'max']}
      />
    )
    const wrapperLargePad = mount(
      <CalendarCell
        data={data}
        metricsName={testPadMetricsName}
        show={['min', 'mean', 'max']}
      />
    )
    const smallPadExample = wrapperSmallPad.find('.CalendarCell-mean').at(0)
    const largePadExample = wrapperLargePad.find('.CalendarCell-mean').at(0)
    expect(smallPadExample.text()).toHaveLength(6)
    expect(largePadExample.text()).toHaveLength(7)
  })
})
