import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'antd'
const { dialog } = window.require('electron').remote

/**
 * @typedef {Object} OpenButtonProps
 * @property {function(string)} onSelectPath
 */

/**
 * @param {OpenButtonProps} props
 * @returns {React.Component.<OpenButtonProps>}
 */
function OpenButton (props) {
  const { onSelectPath } = props

  const onClick = () => {
    dialog.showOpenDialog({ properties: ['openDirectory'] }, (paths) => {
      if (paths && paths.length === 1) {
        onSelectPath(paths[0])
      }
    })
  }

  return (
    <Button
      type="primary"
      icon="folder-open"
      onClick={onClick}
    >
      Открыть
    </Button>
  )
}

OpenButton.propTypes = {
  onSelectPath: PropTypes.func.isRequired
}

export default OpenButton
