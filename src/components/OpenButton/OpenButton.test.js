import React from 'react'
import { shallow } from 'enzyme'
import OpenButton from './OpenButton'
const { dialog } = window.require('electron').remote

describe('<OpenButton />', () => {
  it('calls `showOpenDialog` on click', () => {
    const onSelectPath = jest.fn()
    const wrapper = shallow(<OpenButton onSelectPath={onSelectPath} />)
    wrapper.simulate('click')
    expect(dialog.showOpenDialog).toHaveBeenCalledTimes(1)
  })

  it('selects the path specified by the user', () => {
    dialog.showOpenDialogPaths = ['/some/path']
    const onSelectPath = jest.fn()
    const wrapper = shallow(<OpenButton onSelectPath={onSelectPath} />)
    wrapper.simulate('click')
    expect(onSelectPath).toHaveBeenCalledTimes(1)
    expect(onSelectPath).toHaveBeenLastCalledWith('/some/path')
  })

  it('doesn\'t select anything if the user did not specify a path', () => {
    dialog.showOpenDialogPaths = []
    const onSelectPath = jest.fn()
    const wrapper = shallow(<OpenButton onSelectPath={onSelectPath} />)
    wrapper.simulate('click')
    expect(onSelectPath).not.toHaveBeenCalled()
  })
})
