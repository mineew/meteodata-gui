import React from 'react'
import PropTypes from 'prop-types'
import { Select, InputNumber } from 'antd'
import './LatLong.css'

/**
 * @typedef {Object} Location
 * @property {string} name
 * @property {number} latitude
 * @property {number} longitude
 */

/**
 * @typedef {Object} LatLongProps
 * @property {Array.<Location>} locations
 * @property {[number, number]} value
 * @property {function(number, number)} onChange
 */

/**
 * @param {LatLongProps} props
 * @returns {React.Component<LatLongProps>}
 */
function LatLong (props) {
  const { locations, value, onChange } = props

  const selectOptions = locations.map(({ name }) => name)
  const selectValue = _getLocationName(locations, value) || undefined
  const [ latitude, longitude ] = value

  const handleSelectChange = (name) => {
    const coords = _getLocationCoords(locations, name)
    if (coords) {
      onChange(...coords)
    }
  }

  const handleLatitudeChange = (latitude) => {
    onChange(Number(latitude), longitude)
  }

  const handleLongitudeChange = (longitude) => {
    onChange(latitude, Number(longitude))
  }

  return (
    <div className="LatLong">
      <Select
        placeholder="Выберите место установки метеостанции"
        value={selectValue}
        onChange={handleSelectChange}
      >
        {selectOptions.map((opt) => (
          <Select.Option key={opt} value={opt}>
            {opt}
          </Select.Option>
        ))}
      </Select>
      <InputNumber
        placeholder="Широта"
        value={latitude}
        onChange={handleLatitudeChange}
      />
      <InputNumber
        placeholder="Долгота"
        value={longitude}
        onChange={handleLongitudeChange}
      />
    </div>
  )
}

LatLong.propTypes = {
  locations: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired
  })).isRequired,
  value: PropTypes.arrayOf(PropTypes.number).isRequired,
  onChange: PropTypes.func.isRequired
}

export default LatLong

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Array.<Location>} locations
 * @param {[number, number]} coords
 * @returns {string}
 */
function _getLocationName (locations, coords) {
  const [ latitude, longitude ] = coords
  for (let location of locations) {
    if (location.latitude === latitude && location.longitude === longitude) {
      return location.name
    }
  }
  return ''
}

/**
 * @param {Array.<Location>} locations
 * @param {string} name
 * @returns {[number, number]|null}
 */
function _getLocationCoords (locations, name) {
  for (let location of locations) {
    if (location.name === name) {
      return [location.latitude, location.longitude]
    }
  }
  return null
}
