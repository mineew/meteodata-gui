import React from 'react'
import { mount } from 'enzyme'
import { Select } from 'antd'
import LatLong from './LatLong'

const locations = [{
  name: 'Location 1',
  latitude: 11,
  longitude: 12
}, {
  name: 'Location 2',
  latitude: 21,
  longitude: 22
}, {
  name: 'Location 3',
  latitude: 31,
  longitude: 32
}]

describe('<LatLong />', () => {
  it('shows the matching location', () => {
    const wrapper = mount(
      <LatLong
        locations={locations}
        value={[21, 22]}
        onChange={jest.fn()}
      />
    )
    expect(wrapper.text()).toMatch('Location 2')
  })

  it('shows empty select if there is no matching location', () => {
    const wrapper = mount(
      <LatLong
        locations={locations}
        value={[1, 2]}
        onChange={jest.fn()}
      />
    )
    expect(wrapper.text()).not.toMatch(locations[0].name)
    expect(wrapper.text()).not.toMatch(locations[1].name)
    expect(wrapper.text()).not.toMatch(locations[2].name)
  })

  it('changes the latitude', async () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <LatLong
        locations={locations}
        value={[11, 12]}
        onChange={onChange}
      />
    )
    wrapper.find('.ant-input-number-input').at(0).simulate('change', {
      target: { value: '13' }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(13, 12)
  })

  it('changes the longitude', async () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <LatLong
        locations={locations}
        value={[11, 12]}
        onChange={onChange}
      />
    )
    wrapper.find('.ant-input-number-input').at(1).simulate('change', {
      target: { value: '13' }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(11, 13)
  })

  it('changes the coords with the select', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <LatLong
        locations={locations}
        value={[11, 12]}
        onChange={onChange}
      />
    )
    wrapper.find(Select).instance().props.onChange('Location 2')
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(21, 22)
  })

  it('doesn\'t change the coords with the select if there is no matching location', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <LatLong
        locations={locations}
        value={[11, 12]}
        onChange={onChange}
      />
    )
    wrapper.find(Select).instance().props.onChange('Location N')
    expect(onChange).toHaveBeenCalledTimes(0)
  })
})
