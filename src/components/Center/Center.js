import React from 'react'
import PropTypes from 'prop-types'
import './Center.css'

/**
 * @typedef {Object} CenterProps
 * @property {React.ReactNode} children
 */

/**
 * @param {CenterProps} props
 * @returns {React.Component.<CenterProps>}
 */
function Center (props) {
  const { children } = props

  return (
    <div className="Center fullscreen">
      {children}
    </div>
  )
}

Center.propTypes = {
  children: PropTypes.node.isRequired
}

export default Center
