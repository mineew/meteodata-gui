import React from 'react'
import { shallow } from 'enzyme'
import Center from './Center'

describe('<Center />', () => {
  it('renders children', () => {
    const wrapper = shallow(
      <Center>
        <p>children</p>
      </Center>
    )
    expect(wrapper.text()).toMatch('children')
  })
})
