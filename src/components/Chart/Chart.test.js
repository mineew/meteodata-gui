import React from 'react'
import { shallow } from 'enzyme'
import { Line, Area, Brush, ReferenceLine, Tooltip } from 'recharts'
import Chart from './Chart'

const titles = ['DataSet 1', 'DataSet 2']
const colors = ['red', 'green']
const data = [
  [
    { name: '01', min: 110, mean: 111, max: 112 },
    { name: '02', min: 120, mean: 121, max: 122 },
    { name: '02', min: 130, mean: 131, max: 132 }
  ],
  [
    { name: '01', min: 210, mean: 211, max: 212 },
    { name: '02', min: 220, mean: 221, max: 222 },
    { name: '02', min: 230, mean: 231, max: 232 }
  ]
]
const aLotOfData = [Array(120).fill(data[0][0])]
const dataWithNegativeValues = [
  data[0],
  [
    data[1][0],
    data[1][1],
    { name: '02', min: -230, mean: 231, max: 232 }
  ]
]
const dataWithTooSmallRange = [[
  { name: '01', min: 0, mean: 0.1, max: 0.2 },
  { name: '01', min: 0.03, mean: 0.04, max: 0.05 }
]]

const getWrapper = (d = data) => shallow(
  <Chart titles={titles} colors={colors} data={d} />
)

describe('<Chart />', () => {
  it('shows the first dataset by default', () => {
    const wrapper = getWrapper()
    expect(wrapper.find(Line)).toHaveLength(1)
    expect(wrapper.find(Area)).toHaveLength(1)
  })

  it('can toogle datasets', () => {
    const wrapper = getWrapper()
    wrapper.instance().handleSelectDataSetsChange(titles)
    expect(wrapper.find(Line)).toHaveLength(2)
    expect(wrapper.find(Area)).toHaveLength(2)
    wrapper.instance().handleSelectDataSetsChange([titles[1]])
    expect(wrapper.find(Line)).toHaveLength(1)
    expect(wrapper.find(Area)).toHaveLength(1)
  })

  it('cannot disable all datasets', () => {
    const wrapper = getWrapper()
    wrapper.instance().handleSelectDataSetsChange([])
    expect(wrapper.find(Line)).toHaveLength(1)
    expect(wrapper.find(Area)).toHaveLength(1)
  })

  it('can change chart type', () => {
    const wrapper = getWrapper()
    wrapper.instance().handleChartTypeChange('basis')
    expect(wrapper.state('type')).toBe('basis')
  })

  it('can disable areas', () => {
    const wrapper = getWrapper()
    wrapper.instance().handleShowAreasChange(false)
    expect(wrapper.find(Line)).toHaveLength(1)
    expect(wrapper.find(Area)).toHaveLength(0)
  })

  it('doesn\'t show areas if the range is too small', () => {
    const wrapper = getWrapper(dataWithTooSmallRange)
    expect(wrapper.find(Line)).toHaveLength(1)
    expect(wrapper.find(Area)).toHaveLength(0)
  })

  it('shows the brush if there is a lot of data', () => {
    const wrapper = getWrapper(aLotOfData)
    expect(wrapper.find(Brush)).toHaveLength(1)
  })

  it('shows the reference zero if there is negative values', () => {
    const wrapper = getWrapper(dataWithNegativeValues)
    expect(wrapper.find(ReferenceLine)).toHaveLength(1)
  })

  it('shows values in tooltip', () => {
    const wrapper = getWrapper()
    expect(wrapper.find(Tooltip)).toHaveLength(1)
  })

  it('formats values correctly', () => {
    const wrapper = getWrapper()
    const formatter = wrapper.find(Tooltip).prop('formatter')
    expect(formatter(null)).toBe('-')
    expect(formatter(1.12311)).toBe('1.12')
    expect(formatter([null, null])).toBe('- ~ -')
    expect(formatter([1.12311, null])).toBe('1.12 ~ -')
    expect(formatter([null, 1.12311])).toBe('- ~ 1.12')
    expect(formatter([1.12311, 1.12311])).toBe('1.12 ~ 1.12')
  })
})
