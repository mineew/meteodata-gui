import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Select, Switch, Divider } from 'antd'
import {
  ResponsiveContainer,
  ComposedChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Line,
  Area,
  Tooltip,
  ReferenceLine,
  Brush
} from 'recharts'
import Checkboxes from '../Checkboxes/Checkboxes'
import './Chart.css'

const CHART_TYPES = [
  'basis',
  'linear',
  'monotone',
  'natural',
  'step'
]

/**
 * @typedef {Object} ChartProps
 * @property {Array.<string>} titles
 * @property {Array.<string>} colors
 * @property {Array.<Array.<object>>} data
 * @property {string} [metricsName]
 * @property {number|string} [width]
 * @property {number|string} [height]
 * @property {number} [areasThreshold]
 */

/**
 * @augments {React.Component<ChartProps>}
 */
class Chart extends PureComponent {
  state = {
    selectedDataSets: [this.props.titles[0]],
    visibleDataSets: [0],
    showAreas: _needToShowAreas(this.props.data, this.props.areasThreshold),
    type: 'monotone'
  }

  render () {
    const { titles, colors, data, metricsName, width, height } = this.props
    const { selectedDataSets, visibleDataSets, showAreas, type } = this.state

    const meanTitle = metricsName === 'rain' ? 'сумма' : 'среднее'

    return (
      <div className="Chart">
        <div className="Chart-toolbox">
          {titles.length > 1 && (
            <Fragment>
              <Checkboxes
                options={titles}
                value={selectedDataSets}
                onChange={this.handleSelectDataSetsChange}
              />
              <Divider type="vertical" />
            </Fragment>
          )}
          <Select value={type} onChange={this.handleChartTypeChange}>
            {CHART_TYPES.map((t) => (
              <Select.Option key={t} value={t}>{t}</Select.Option>
            ))}
          </Select>
          <Switch checked={showAreas} onChange={this.handleShowAreasChange} />
          Показывать области
        </div>
        <ResponsiveContainer width={width} height={height}>
          <ComposedChart
            data={_flat(data, titles, metricsName)}
            margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
          >
            <CartesianGrid strokeDasharray="5 5" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip formatter={_formatValue} />
            {_needToShowReferenceZero(data) && (
              <ReferenceLine y={0} stroke="#666" />
            )}
            {_heedToShowBrush(data) && (
              <Brush dataKey="name" {..._brushPosition(data)} />
            )}
            {data.map((_, i) => visibleDataSets.includes(i) && (
              <Line
                key={`${titles[i]} линия`}
                dataKey={`${titles[i]} (${meanTitle})`}
                stroke={colors[i]}
                type={type}
              />
            ))}
            {showAreas && data.map((_, i) => visibleDataSets.includes(i) && (
              <Area
                key={`${titles[i]} область`}
                dataKey={`${titles[i]} (мин, макс)`}
                stroke={colors[i]}
                strokeOpacity={0.4}
                fill={colors[i]}
                fillOpacity={0.2}
                type={type}
              />
            ))}
          </ComposedChart>
        </ResponsiveContainer>
      </div>
    )
  }

  handleSelectDataSetsChange = (selectedDataSets) => {
    if (!selectedDataSets.length) {
      return
    }
    const { titles } = this.props
    this.setState({
      selectedDataSets,
      visibleDataSets: titles.map(
        (name, i) => selectedDataSets.includes(name) ? i : null
      ).filter((n) => n !== null)
    })
  }

  handleChartTypeChange = (type) => {
    this.setState({ type })
  }

  handleShowAreasChange = (showAreas) => {
    this.setState({ showAreas })
  }
}

Chart.defaultProps = {
  metricsName: '',
  width: '100%',
  height: 300,
  areasThreshold: 1
}

Chart.propTypes = {
  titles: PropTypes.arrayOf(PropTypes.string).isRequired,
  colors: PropTypes.arrayOf(PropTypes.string).isRequired,
  data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    mean: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number
  }))).isRequired,
  metricsName: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  areasThreshold: PropTypes.number
}

export default Chart

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Array.<Array.<object>>} data
 * @param {Array.<string>} titles
 * @param {string} metricsName
 * @returns {Array.<object>}
 */
function _flat (data, titles, metricsName) {
  const names = _getNames(data)
  const meanTitle = metricsName === 'rain' ? 'сумма' : 'среднее'
  const flatData = names.map((name) => ({ name }))
  for (let i = 0; i < data.length; i++) {
    for (let dataItem of _addRange(data[i])) {
      const flatItem = _findByName(flatData, dataItem.name)
      flatItem[`${titles[i]} (${meanTitle})`] = dataItem.mean
      flatItem[`${titles[i]} (мин, макс)`] = dataItem.range
    }
  }
  return flatData
}

/**
 * @private
 * @param {Array.<Array.<object>>} data
 * @returns {Array.<string>}
 */
function _getNames (data) {
  const names = []
  for (let dataSet of data) {
    for (let dataItem of dataSet) {
      if (!names.includes(dataItem.name)) {
        names.push(dataItem.name)
      }
    }
  }
  return names
}

/**
 * @private
 * @param {Array.<object>} dataSet
 * @param {string} name
 * @returns {object}
 */
function _findByName (dataSet, name) {
  return dataSet.find((dataItem) => dataItem.name === name)
}

/**
 * @private
 * @param {Array.<object>} dataSet
 * @returns {Array.<object>}
 */
function _addRange (dataSet) {
  return dataSet.map((i) => {
    let range = [i.min, i.max]
    if (i.min === null || i.max === null) {
      range = []
    }
    return { ...i, range }
  })
}

/**
 * @private
 * @param {Array.<Array.<object>>} data
 * @param {number} threshold
 * @returns {boolean}
 */
function _needToShowAreas (data, threshold) {
  for (let dataSet of data) {
    for (let dataItem of dataSet) {
      const diff = Math.abs(dataItem.min - dataItem.max)
      if (diff >= threshold * 2) {
        return true
      }
    }
  }
  return false
}

/**
 * @private
 * @param {Array.<Array.<object>>} data
 * @returns {boolean}
 */
function _needToShowReferenceZero (data) {
  for (let dataSet of data) {
    for (let dataItem of dataSet) {
      if (dataItem.min < 0 || dataItem.mean < 0 || dataItem.max < 0) {
        return true
      }
    }
  }
  return false
}

/**
 * @private
 * @param {number|null|[number, number]|[null, null]} value
 * @returns {string}
 */
function _formatValue (value) {
  const dashes = [null, undefined]
  if (Array.isArray(value)) {
    const [ min, max ] = value
    const minf = dashes.includes(min) ? '-' : min.toFixed(2)
    const maxf = dashes.includes(max) ? '-' : max.toFixed(2)
    return `${minf} ~ ${maxf}`
  }
  return dashes.includes(value) ? '-' : value.toFixed(2)
}

/**
 * @private
 * @param {Array.<Array.<object>>} data
 * @returns {{startIndex: number, endIndex: number}}
 */
function _brushPosition (data) {
  const endIndex = data.filter((dataSet) => dataSet.length >= 100).length
    ? 20
    : undefined
  return { startIndex: 0, endIndex }
}

/**
 * @private
 * @param {Array.<Array.<object>>} data
 * @returns {boolean}
 */
function _heedToShowBrush (data) {
  const position = _brushPosition(data)
  return Boolean(position.endIndex)
}
