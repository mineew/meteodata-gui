import React from 'react'
import { mount } from 'enzyme'
import Menu from './Menu'

const props = {
  options: [
    ['name1', 'key1'],
    ['name2', 'key2'],
    ['name3', 'key3']
  ],
  value: 'key2',
  onChange: jest.fn()
}

describe('<Menu />', () => {
  it('renders options', () => {
    const wrapper = mount(<Menu {...props} />)
    for (let i = 0; i < props.options.length; i++) {
      const text = props.options[i][0]
      expect(wrapper.find('.ant-menu-item').at(i).text()).toBe(text)
    }
  })

  it('selects a value', () => {
    const wrapper = mount(<Menu {...props} />)
    expect(wrapper.find('.ant-menu-item-selected').text()).toBe('name2')
  })

  it('changes a value', () => {
    const wrapper = mount(<Menu {...props} />)
    wrapper.find('.ant-menu-item').at(0).simulate('click')
    expect(props.onChange).toHaveBeenCalledTimes(1)
    expect(props.onChange).toHaveBeenLastCalledWith('key1')
  })
})
