import React from 'react'
import PropTypes from 'prop-types'
import { Menu as AntdMenu } from 'antd'

/**
 * @typedef {Object} MenuProps
 * @property {Array.<[string, string]>} options
 * @property {string} value
 * @property {function(string)} onChange
 */

/**
 * @param {MenuProps} props
 * @returns {React.Component<MenuProps>}
 */
function Menu (props) {
  const { options, value, onChange } = props

  return (
    <AntdMenu
      selectedKeys={[value]}
      onSelect={({ key }) => onChange(key)}
    >
      {options.map(([ name, key ]) => (
        <AntdMenu.Item key={key}>{name}</AntdMenu.Item>
      ))}
    </AntdMenu>
  )
}

Menu.propTypes = {
  options: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)).isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

export default Menu
