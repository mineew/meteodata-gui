import React from 'react'
import PropTypes from 'prop-types'
import { Checkbox } from 'antd'
import './Checkboxes.css'

/**
 * @typedef {Object} CheckboxesProps
 * @property {Array.<string>} options
 * @property {Array.<string>} [value]
 * @property {function(Array.<string>)} [onChange]
 * @property {boolean} [showCheckAll]
 */

/**
 * @param {CheckboxesProps} props
 * @returns {React.Component<CheckboxesProps>}
 */
function Checkboxes (props) {
  const { options, value, onChange, showCheckAll } = props

  const checkAllIndeterminate = value.length && (
    value.length < options.length
  )
  const checkAllChecked = value.length === options.length

  const handleCheckAllChange = !onChange ? undefined : (e) => {
    const { checked } = e.target
    onChange(checked ? options : [])
  }

  const handleChange = !onChange ? undefined : (checkedOptions) => {
    onChange(checkedOptions)
  }

  return (
    <div className="Checkboxes">
      {showCheckAll && (
        <div className="Checkboxes-check-all">
          <Checkbox
            indeterminate={checkAllIndeterminate}
            checked={checkAllChecked}
            onChange={handleCheckAllChange}
          >
            Выбрать все
          </Checkbox>
        </div>
      )}
      <Checkbox.Group
        options={options}
        value={value}
        onChange={handleChange}
      />
    </div>
  )
}

Checkboxes.defaultProps = {
  value: []
}

Checkboxes.propTypes = {
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  value: PropTypes.arrayOf(PropTypes.string),
  onChange: PropTypes.func,
  showCheckAll: PropTypes.bool
}

export default Checkboxes
