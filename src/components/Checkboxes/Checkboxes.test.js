import React from 'react'
import { mount } from 'enzyme'
import Checkboxes from './Checkboxes'

describe('<Checkboxes />', () => {
  it('renders options', () => {
    const options = ['option 1', 'option 2', 'option 3']
    const wrapper = mount(
      <Checkboxes options={options} />
    )
    expect([
      wrapper.find('.ant-checkbox-input').at(0).props().value,
      wrapper.find('.ant-checkbox-input').at(1).props().value,
      wrapper.find('.ant-checkbox-input').at(2).props().value
    ]).toEqual(options)
  })

  it('changes value', () => {
    const options = ['option 1', 'option 2', 'option 3']
    const onChange = jest.fn()
    const wrapper = mount(
      <Checkboxes options={options} onChange={onChange} />
    )
    wrapper.find('.ant-checkbox-input').at(0).simulate('change', {
      target: { checked: true }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(['option 1'])
  })

  it('can check all', () => {
    const options = ['option 1', 'option 2', 'option 3']
    const onChange = jest.fn()
    const wrapper = mount(
      <Checkboxes
        showCheckAll={true}
        options={options}
        onChange={onChange}
      />
    )
    wrapper.find('.ant-checkbox-input').at(0).simulate('change', {
      target: { checked: true }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(options)
    wrapper.find('.ant-checkbox-input').at(0).simulate('change', {
      target: { checked: false }
    })
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange).toHaveBeenLastCalledWith([])
  })

  it('can show indeterminate state', () => {
    const options = ['option 1', 'option 2', 'option 3']
    const wrapper = mount(
      <Checkboxes
        showCheckAll={true}
        options={options}
        value={['option 1']}
      />
    )
    expect(wrapper.html()).toMatch('ant-checkbox-indeterminate')
  })
})
