import React from 'react'
import PropTypes from 'prop-types'
import { Tabs as AntdTabs, Icon } from 'antd'
import './Tabs.css'

/**
 * @typedef {Object} TabsProps
 * @property {Array.<string>} titles
 * @property {Array.<string>} icons
 * @property {Array.<React.ReactNode>} panes
 */

/**
 * @param {TabsProps} props
 * @returns {React.Component.<TabsProps>}
 */
export function Tabs (props) {
  const { titles, icons, panes } = props

  return (
    <div className="Tabs">
      <AntdTabs type="card">
        {panes.map((pane, i) => (
          <AntdTabs.TabPane
            key={titles[i]}
            tab={(
              <span>
                <Icon type={icons[i]} /> {titles[i]}
              </span>
            )}
          >
            {pane}
          </AntdTabs.TabPane>
        ))}
      </AntdTabs>
    </div>
  )
}

Tabs.propTypes = {
  titles: PropTypes.arrayOf(PropTypes.string).isRequired,
  icons: PropTypes.arrayOf(PropTypes.string).isRequired,
  panes: PropTypes.arrayOf(PropTypes.node).isRequired
}

export default Tabs
