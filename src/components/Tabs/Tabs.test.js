import React from 'react'
import { shallow, mount } from 'enzyme'
import Tabs from './Tabs'

const props = {
  titles: ['Title 1', 'Title 2'],
  icons: ['plus', 'minus'],
  panes: [
    <p>Pane 1</p>,
    <p>Pane 2</p>
  ]
}

describe('<Tabs />', () => {
  it('renders titles', () => {
    const wrapper = shallow(<Tabs {...props} />)
    expect(wrapper.html()).toMatch(props.titles[0])
    expect(wrapper.html()).toMatch(props.titles[1])
  })

  it('renders icons', () => {
    const wrapper = shallow(<Tabs {...props} />)
    expect(wrapper.html()).toMatch(`anticon-${props.icons[0]}`)
    expect(wrapper.html()).toMatch(`anticon-${props.icons[1]}`)
  })

  it('renders panes', () => {
    const wrapper = mount(<Tabs {...props} />)
    expect(wrapper.html()).toMatch(shallow(props.panes[0]).html())
    wrapper.find('.ant-tabs-tab').at(1).simulate('click')
    expect(wrapper.html()).toMatch(shallow(props.panes[1]).html())
  })
})
