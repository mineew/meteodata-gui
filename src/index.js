import React, { Fragment, StrictMode } from 'react'
import ReactDOM from 'react-dom'
import { LocaleProvider } from 'antd'
import ruRU from 'antd/lib/locale-provider/ru_RU'
import { ApolloProvider } from 'react-apollo'
import client from './client'
import ErrorBoundary from './ErrorBoundary'
import App from './App'

import 'antd/dist/antd.css'

import moment from 'moment'
import 'moment/locale/ru'
moment.locale('ru')

function RenderApp () {
  const { REACT_APP_MODE } = process.env
  let AppWrapper = Fragment
  if (REACT_APP_MODE === 'strict') {
    AppWrapper = StrictMode
  }
  return (
    <AppWrapper>
      <ErrorBoundary>
        <App />
      </ErrorBoundary>
    </AppWrapper>
  )
}

ReactDOM.render(
  <ApolloProvider client={client}>
    <LocaleProvider locale={ruRU}>
      <RenderApp />
    </LocaleProvider>
  </ApolloProvider>,
  document.getElementById('root')
)
