/* eslint-disable no-console */

import { ApolloClient } from 'apollo-client'
import { onError } from 'apollo-link-error'
import { HttpLink } from 'apollo-link-http'
import { ApolloLink } from 'apollo-link'
import {
  InMemoryCache,
  IntrospectionFragmentMatcher
} from 'apollo-cache-inmemory'
import introspectionQueryResultData from './fragmentTypes.json'

import config from './config'

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach((error) => {
      const { message: m, locations: l, path: p } = error
      console.log(
        `[GraphQL error]: Message: ${m}, Location: ${l}, Path: ${p}`
      )
    })
  }
  if (networkError) {
    console.log(`[Network error]: ${networkError}`)
  }
})

const httpLink = new HttpLink({
  uri: config.serverUrl
})

export const cache = new InMemoryCache({
  fragmentMatcher: new IntrospectionFragmentMatcher({
    introspectionQueryResultData
  })
})

const client = new ApolloClient({
  link: ApolloLink.from([errorLink, httpLink]),
  cache
})

export default client
