import React, { PureComponent } from 'react'
import { Center, Tabs, AppInfo } from './components'
import {
  SetRootCard,
  LoadDatabaseCard,
  SetLatLongCard,
  Calendar,
  Table
} from './containers'
import {
  searchComplete,
  invalidPathsDetected,
  closeNotifications
} from './notifications'
import './App.css'

/**
 * @typedef {import('./containers').RootInfo} RootInfo
 */

/**
 * @typedef {import('./containers').DatabaseInfo} DatabaseInfo
 */

class App extends PureComponent {
  state = {
    root: undefined,
    filesCount: 0,
    invalidPaths: [],
    years: [],
    metricsCount: 0,
    missedMetricsCount: 0,
    brokenMetricsCount: 0,
    minDate: undefined,
    maxDate: undefined,
    latitude: 0,
    longitude: 0
  }

  componentDidUpdate () {
    const { filesCount, invalidPaths } = this.state
    if (this.rootIsDefined && !this.metricsAreLoaded) {
      searchComplete(filesCount)
      if (invalidPaths.length) {
        setTimeout(() => {
          invalidPathsDetected(invalidPaths)
        })
      }
    }
    if (this.metricsAreLoaded && !this.locationIsDefined) {
      closeNotifications()
    }
  }

  render () {
    const { years, minDate, maxDate } = this.state

    return (
      <div id="App">
        {this.needToSetRoot && (
          <Center>
            <SetRootCard onSetRoot={this.handleSetRoot} />
          </Center>
        )}
        {this.needToLoadMetrics && (
          <Center>
            <LoadDatabaseCard
              years={years}
              onDatabaseLoaded={this.handleDatabaseLoaded}
            />
          </Center>
        )}
        {this.needToSetLatLong && (
          <Center>
            <SetLatLongCard onSetLatLong={this.handleSetLatLong} />
          </Center>
        )}
        {this.appIsReady && (
          <Tabs
            titles={['Календарь', 'Таблица', 'Информация']}
            icons={['calendar', 'table', 'info-circle']}
            panes={[
              <Calendar minDate={minDate} maxDate={maxDate} />,
              <Table minDate={minDate} maxDate={maxDate} />,
              <AppInfo {...this.state} />
            ]}
          />
        )}
      </div>
    )
  }

  /** handlers */

  /**
   * @param {RootInfo} rootInfo
   */
  handleSetRoot = (rootInfo) => {
    this.setState({ ...rootInfo })
  }

  /**
   * @param {DatabaseInfo} databaseInfo
   */
  handleDatabaseLoaded = (databaseInfo) => {
    this.setState({ ...databaseInfo })
  }

  /**
   * @param {number} latitude
   * @param {number} longitude
   */
  handleSetLatLong = (latitude, longitude) => {
    this.setState({ latitude, longitude })
  }

  /** getters */

  /**
   * @returns {boolean}
   */
  get rootIsDefined () {
    const { root } = this.state
    return Boolean(root)
  }

  /**
   * @returns {boolean}
   */
  get metricsAreLoaded () {
    const { metricsCount } = this.state
    return Boolean(metricsCount)
  }

  /**
   * @returns {boolean}
   */
  get locationIsDefined () {
    const { latitude, longitude } = this.state
    return Boolean(latitude && longitude)
  }

  /**
   * @returns {boolean}
   */
  get needToSetRoot () {
    return !this.rootIsDefined
  }

  /**
   * @returns {boolean}
   */
  get needToLoadMetrics () {
    return this.rootIsDefined && !this.metricsAreLoaded
  }

  /**
   * @returns {boolean}
   */
  get needToSetLatLong () {
    return (
      this.rootIsDefined &&
      this.metricsAreLoaded &&
      !this.locationIsDefined
    )
  }

  /**
   * @returns {boolean}
   */
  get appIsReady () {
    return (
      this.rootIsDefined &&
      this.metricsAreLoaded &&
      this.locationIsDefined
    )
  }
}

export default App
