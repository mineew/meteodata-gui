import React, { Component } from 'react'
import { Alert } from 'antd'
import { Center } from './components'

class ErrorBoundary extends Component {
  state = {
    error: null
  }

  static getDerivedStateFromError (error) {
    return { error }
  }

  render () {
    const { error } = this.state

    return !error ? this.props.children : (
      <Center>
        <Alert
          message="Произошла ошибка"
          description={error.message}
          type="error"
          showIcon={true}
        />
      </Center>
    )
  }
}

export default ErrorBoundary
