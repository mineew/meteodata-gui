[![pipeline status](https://gitlab.com/mineew/meteodata-gui/badges/master/pipeline.svg)](https://gitlab.com/mineew/meteodata-gui/commits/master)
[![coverage report](https://gitlab.com/mineew/meteodata-gui/badges/master/coverage.svg)](https://gitlab.com/mineew/meteodata-gui/commits/master)

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

# MeteoData

MeteoData is a desktop application built with Electron, GraphQL and React.

![Screenshot](/screenshot.png)

MeteoData reads the weather station source data from `.DAT` files and displays
it in a calendar, table and chart views. MeteoData can also export the source
data to `.xlsx` files which can be opened in Excel.

## Install and run

To install and run the application, type the following in your terminal:

```
~ $ mkdir meteodata && cd meteodata
~ $ git clone https://gitlab.com/mineew/meteodata-gui.git .
~ $ yarn
~ $ yarn start
```

## Example data

If you want to look at some example data, run the following in your terminal:

```
~ $ yarn example
```

This will create the `example` folder with the `.DAT` files.

## All available scripts

| NPM Script     | Description                   |
| -------------- | ----------------------------- |
| `yarn start`   | runs the application          |
| `yarn test`    | tests the application         |
| `yarn cover`   | shows the code coverage       |
| `yarn lint`    | lints the code                |
| `yarn build`   | creates a production build    |
| `yarn clear`   | removes the temporary folders |
| `yarn example` | extracts the example data     |


## Frameworks and tools

This application is built with the following frameworks and tools:

* [Electron](https://electronjs.org/) - Build cross platform desktop apps with
  JavaScript, HTML, and CSS;
* [React](https://reactjs.org/) - A JavaScript library for building user
  interfaces;
* [Create React App](https://github.com/facebook/create-react-app) - create
  react apps with no build configuration;
* [Ant Design of React](https://ant.design/docs/react/introduce) - a React UI
  library that contains a set of high quality components and demos for building
  rich, interactive user interfaces;
* [GraphQL](https://graphql.org/) - A query language for your API;
* [Apollo Client](https://github.com/apollographql/apollo-client) -
  A fully-featured, production ready caching GraphQL client;
* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web
  framework for Node.js.

The tests are written with these tools:

* [Jest](https://jestjs.io/) - Delightful JavaScript Testing;
* [Enzyme](http://airbnb.io/enzyme/) - a JavaScript Testing utility for React.

The code style is managed by:

* [ESlint](https://eslint.org/) - The pluggable linting utility for JavaScript
  and JSX;
* [eslint-config-standard](https://github.com/standard/eslint-config-standard) -
  ESLint Config for JavaScript Standard Style.

Other tools worth mentioning:

* [Recharts](http://recharts.org/) - A composable charting library built on
  React components;
* [ExcelJS](https://github.com/exceljs/exceljs) - Read, manipulate and write
  spreadsheet data and styles to XLSX and JSON;
* [SunCalc](https://github.com/mourner/suncalc) - a tiny BSD-licensed
  JavaScript library for calculating sun position;
* [Moment.js](https://momentjs.com/) - Parse, validate, manipulate, and display
  dates and times in JavaScript.
