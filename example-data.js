const fs = require('fs')
const unzipper = require('unzipper')

fs.createReadStream('example-data.zip').pipe(unzipper.Extract({
  path: 'example'
}))
